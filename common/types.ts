export interface User {
    id: number;
    username: string;
    authority_level: number;
    department_id: number;
    // password: string;
}

export interface HomePageSummary {
    staffName: string;
}

declare global {
    namespace Express {
        interface Request {
            user: User;
        }
    }
}

export interface ManagementRecord{
    staffName: string;
    
}

export interface StaffDetail{
    username:string;
    id?:number;
    department_id:number;
    authority_level:number;
    itemKey:number;
    department:string;
    checkin_time:string;
    leave_type:string;
    department_name:string; 
    salary:number;
    start_time:Date;
    end_time:Date;
    vacation_name:string;
}

export interface onTimeRecord{
    staffsDetail:StaffDetail[]
}

export interface recordType{
   [username:string]:string;
}

