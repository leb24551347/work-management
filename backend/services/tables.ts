const tables = Object.freeze({  
    sessions: 'sessions',
    department:'department',
    staff:'staff',
    staff_registration:'staff_registration',
    ot_allowance_level:'ot_allowance_level',
    staff_performance:'staff_performance',
    staff_salary:'staff_salary',
    staff_leave:'staff_leave',
    staff_ot:'staff_ot',
    vacation_type:'vacation_type',
    heart_beat: 'heart_beat',
});

export default tables;
