import Knex from 'knex';
import tables from './tables';
import { User } from '../../common';
import { checkPassword, hashPassword } from '../hash';

export class UserService {
    // Dependency Injection
    public constructor(private knex: Knex) {}

    // async getUserInfo(userID: number) {
    //     const user = await this.knex<User>(tables.STAFF).where('id', userID).first();
    //     return user;
    // }

    async getUserByUserName(username: string) {
        const user = await this.knex<User>(tables.staff).where('username', username).first();
        // const user = await this.knex<User>(tables.USERS).select('department_id').from('staff').where('username', username).first();
        return user;
    }

    async createUser(username: string, password: string, departments: string, authorityLevel: string) {
        const hashedPassword = await hashPassword(password);
        const [userID] = await this.knex(tables.staff).insert({
            username: username,
            password: hashedPassword,
            department_id: departments,
            authority_level: authorityLevel,
        });
        return userID;
    }

    public checkPassword = async (userId: number, password: string) => {
        const user = await this.knex(tables.staff).where('id', userId).select('password').first();

        if (!user) {
            return false;
        }

        if (!(await checkPassword(password, user.password))) {
            return false;
        }

        return true;
    };

    public changePassword = async (userId: number, password: string, newPassword: string) => {
        if (!(await this.checkPassword(userId, password))) {
            return false;
        }

        const hashedPassword = await hashPassword(newPassword);
        await this.knex(tables.staff).where('id', '=', userId).update({ password: hashedPassword });

        return true;
    };

    public department = async () => {
        const dep = await this.knex.select('id', 'department_name').from('department');
        console.log(dep);

        return dep;
    };
    public getUser = async (id: string) => {
        const user = await this.knex<User>(tables.staff).where('id', id).select('authority_level', 'department_id', 'username', 'id');
        // console.log('clockuser: ', user);
        return user[0];
    };
}
