import Knex from "knex";
import moment from "moment";
import tables from "./tables";


export class HomePageService {

    public constructor(private knex: Knex){
        
    }



    async getEmployeeDetail(userId: number, startDate: string) {
    console.log("🚀 ~ file: HomePageService.ts ~ line 15 ~ HomePageService ~ getEmployeeDetail ~ startDate", startDate)

    const startDateMoment = moment('2021-' + startDate + '-01', 'YYYY-M-DD').format('YYYY-MM-DD HH:mm:ssZ')
    const endDateMoment = moment('2021-' + startDate + '-01', 'YYYY-M-DD').add(1, 'month').format('YYYY-MM-DD HH:mm:ssZ')
    console.log("🚀 ~ file: HomePageService.ts ~ line 33 ~ HomePageService ~ getEmployeeDetail ~ startDateMoment", startDateMoment)
    console.log("🚀 ~ file: HomePageService.ts ~ line 35 ~ HomePageService ~ getEmployeeDetail ~ endDateMoment", endDateMoment)
        
        
        const employeeDetail = await this.knex(tables.staff)
        
        .innerJoin("department", "staff.department_id", "=", "department.id")
        .innerJoin("staff_salary", "staff_salary.staff_id", "=", "staff.id")
        .innerJoin("staff_ot", "staff_ot.staff_id", "=", "staff.id")
        .innerJoin("ot_allowance_level", "staff_ot.allowance_id", "=", "ot_allowance_level.id")
        .innerJoin("staff_leave", "staff_leave.staff_id", "=", "staff.id")
        .innerJoin("staff_registration", "staff_registration.staff_id", "=", "staff.id")
        .select ("username", "staff.id", "department_name", "salary", "bonus", "allowance", "staff_leave.start_time", "staff_leave.end_time", "staff_registration.checkin_time", "staff_registration.checkout_time")
    
        .where("staff.id", "=", userId)
        .andWhere('staff_salary.date', '>=', startDateMoment)
        .andWhere('staff_salary.date', '<', endDateMoment)
        .first()
        
     

        


        const duration = await this.knex(tables.staff_registration)
        .select(this.knex.raw(`"checkout_time" - "checkin_time" as duration`))
        .where("staff_id", "=", userId)
        .andWhere('staff_registration.checkin_time', '>=', startDateMoment)
        .andWhere('staff_registration.checkout_time', '<', endDateMoment)
        .first()
        
        console.log("🚀 ~ file: HomePageService.ts ~ line 32 ~ HomePageService ~ getEmployeeDetail ~ duration", duration)
        const leave_duration = await this.knex(tables.staff_leave)
        .select(this.knex.raw(`"end_time" - "start_time" as leave_duration`))
        .where("staff_id", "=", userId)
        .andWhere('staff_leave.start_time', '>=', startDateMoment)
        .andWhere('staff_leave.end_time', '<', endDateMoment)
        .first()

        if(employeeDetail||duration||leave_duration){
            return {...employeeDetail,...duration,...leave_duration};
        }

    };

    // public getMonthInfo = async (userId: number, startDate: string, endDate: string) => {
    //     const staffMonthData = await this.knex(tables.staff_ot)
    //         .select('id', 'start_time', 'end_time')
    //         .where('staff_id', '=', userId)
            
    //     return staffMonthData;
    // };

  

}