import Knex from 'knex';
import tables from './tables';

export class ClockInService {
    public constructor(private knex: Knex) {}

    public getLastClockIn = async (userId: number) => {
        const loading = await this.knex(tables.staff_registration)
            .select('checkin_time', 'checkout_time', 'id', 'on_working')
            .where('staff_id', '=', userId)
            .orderBy('checkin_time', 'desc')
            .first();

        return loading;
    };

    public clockIn = async (time: string, userId: number) => {
        const clockInTime = await this.knex(tables.staff_registration)
            // .orderBy('staff_id', 'desc')
            // .where('staff_id', '=', userId)
            .insert({
                staff_id: userId,
                checkin_time: time,
                checkout_time: null,
                on_working: true,
            })
            .returning('id');
        return clockInTime;
    };

    public clockOut = async (time: string, userId: number, clockInId: number) => {
        const clockOutTime = await this.knex(tables.staff_registration).where('staff_id', '=', userId).andWhere('id', '=', clockInId).update({
            checkout_time: time,
            on_working: false,
        });

        return clockOutTime;
    };

    public loadClockData = async (userId: number) => {
        const loading = await this.knex(tables.staff_registration)
            .select('checkin_time', 'checkout_time', 'id', 'on_working')
            .where('staff_id', '=', userId)
            .orderBy('checkin_time', 'desc')
            .first();
        return loading;
    };
}
