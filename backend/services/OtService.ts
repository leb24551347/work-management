import Knex from 'knex';
import tables from './tables';

export class OtService {
    public constructor(private knex: Knex) {}

    public applyOt = async (userId: number, otStartDay: string, otEndDay: string, otReason: string) => {
        const applyOt = await this.knex(tables.staff_ot).insert({
            staff_id: userId,
            start_time: otStartDay,
            end_time: otEndDay,
            reason: otReason,
            status: 'apply',
        });
        return applyOt;
    };

    public checkOt = async (userId: number, otStartDay: string, otEndDay: string, otReason: string) => {
        const checkOt = await this.knex(tables.staff_ot)
            .select('start_time', 'end_time', 'id', 'reason')
            .where('staff_id', '=', userId)
            .andWhere('reason', '=', otReason)
            .andWhere('end_time', '>', otStartDay)
            .andWhere('start_time', '<', otStartDay)
            .andWhere('end_time', '>', otEndDay)
            .andWhere('start_time', '<', otEndDay);

        return checkOt;
    };

    public getOtStatus = async (userId: number) => {
        const otStatus = await this.knex(tables.staff_ot)
            .select('id', 'start_time', 'end_time', 'created_at', 'updated_at ', 'status', 'reason')
            .where('staff_id', '=', userId);

        console.log('otStatus: ', otStatus);

        return otStatus;
    };

    public getOtData = async (userId: number, startDate: string, endDate: string) => {
        const staffOtData = await this.knex(tables.staff_ot)
            .select('id', 'start_time', 'end_time', 'created_at', 'updated_at ', 'status', 'reason')
            .where('staff_id', '=', userId)
            .andWhere('start_time', '>=', startDate)
            .andWhere('end_time', '<', endDate);
        return staffOtData;
    };
}
