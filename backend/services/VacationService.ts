import Knex from 'knex';
import tables from './tables';

export class VacationService {
    public constructor(private knex: Knex) {}

    public vacation = async () => {
        const vacation_type = await this.knex.select('id', 'vacation_name').from('vacation_type');
        return vacation_type;
    };

    public checkApplyVacation = async (userId: number, vacationId: string, startDay: string, endDay: string) => {
        const checkApplyVacation = await this.knex(tables.staff_leave)
            .select('leave_type', 'status', 'start_time', 'end_time')
            .where('staff_id', '=', userId)
            .andWhere('leave_type', '=', vacationId)
            .andWhere('end_time', '>', startDay)
            .andWhere('start_time', '<', startDay)
            .andWhere('end_time', '>', endDay)
            .andWhere('start_time', '<', endDay);
        return checkApplyVacation;
    };

    public applyVacation = async (userId: number, vacationId: string, startDay: string, endDay: string) => {
        const applyVacation = await this.knex(tables.staff_leave).where('staff_id', '=', userId).insert({
            start_time: startDay,
            end_time: endDay,
            leave_type: vacationId,
            staff_id: userId,
            status: 'apply',
        });

        return applyVacation;
    };

    public getVacationStatus = async (userId: number) => {
        const vacationStatus = await this.knex(tables.staff_leave)

            .select('start_time', 'end_time', 'staff_leave.created_at', 'staff_leave.updated_at ', 'status', 'staff_leave.id', 'vacation_name')
            .innerJoin('vacation_type', 'staff_leave.leave_type', '=', 'vacation_type.id')
            .where('staff_id', '=', userId);

        // console.log('vacationStatus: ', vacationStatus);

        return vacationStatus;
    };

    public getVacationData = async (userId: number, startDate: string, endDate: string) => {
        const secifyApplyVacation = await this.knex(tables.staff_leave)
            .select('start_time', 'end_time', 'staff_leave.created_at', 'staff_leave.updated_at ', 'status', 'staff_leave.id', 'vacation_name')
            .innerJoin('vacation_type', 'staff_leave.leave_type', '=', 'vacation_type.id')
            .where('staff_id', '=', userId)
            .andWhere('start_time', '>=', startDate)
            .andWhere('end_time', '<', endDate);

        return secifyApplyVacation;
    };
}
