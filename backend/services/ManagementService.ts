import Knex from 'knex';
//import { ManagementRecord } from "../../common/types";
import tables from './tables';
import moment from "moment";


export class ManagementService {

   public constructor(private knex: Knex) {

   }
   
   

   async onTimeRecord(date: Date | string) {

      const setTime = moment(date +'09:00:00', 'YYYY-MM-DD HH-mm-ss').format('YYYY-MM-DD HH:mm:ssZ')
      console.log("🚀 ~ file: ManagementService.ts ~ line 18 ~ ManagementService ~ onTimeRecord ~ setTime", setTime)
      

      const onTimeRecord = await this.knex.from(tables.staff)
         .select('username', 'department_name', "authority_level", "checkin_time")
         .join(tables.department, "department.id", "staff.department_id")
         .leftJoin(tables.staff_registration, "staff.id", "staff_registration.staff_id")
         .from(tables.staff)
         .whereRaw(`checkin_time::date  = '${date}'`)
         .andWhereRaw(`checkin_time::time  <= '${setTime}'`);

         console.log("🚀 ~ file: ManagementService.ts ~ line 34 ~ ManagementService ~ onTimeRecord ~ onTimeRecord", onTimeRecord)


      return onTimeRecord;

   }

   async lateRecord(date: Date | string) {

      const setTime = moment(date +'09:00:00', 'YYYY-MM-DD HH-mm-ss').format('YYYY-MM-DD HH:mm:ssZ')

      console.log("🚀 ~ file: ManagementService.ts ~ line 38 ~ ManagementService ~ lateRecord ~ date", date)
      const lateRecord = await this.knex.from(tables.staff)
         .select('username', 'department_name', "authority_level", "checkin_time")
         .join(tables.department, "department.id", "staff.department_id")
         .leftJoin(tables.staff_registration, "staff.id", "staff_registration.staff_id")
         .from(tables.staff)
         .whereRaw(`checkin_time::date  = '${date}'`)
         .andWhereRaw(`checkin_time::time  > '${setTime}'`);
      console.log("🚀 ~ file: ManagementService.ts ~ line 55 ~ ManagementService ~ lateRecord ~ lateRecord", lateRecord)

      return lateRecord;

   }

   async leaveRecord(date: Date | string) {
      console.log("🚀 ~ file: ManagementService.ts ~ line 44 ~ ManagementService ~ leaveRecord ~ date", date)


      const leaveRecord = await this.knex.from(tables.staff)
         .select('username', 'department_name', "vacation_name")
         .join(tables.department, "department.id", "staff.department_id")
         .leftJoin(tables.staff_leave, "staff_leave.staff_id", "staff.id")
         .leftJoin(tables.vacation_type, 'staff_leave.leave_type', 'vacation_type.id')
         .from(tables.staff)
         .where("staff_leave.start_time", "<=", date)
         .andWhere("staff_leave.end_time", ">=", date)
         .andWhere("staff_leave.status", '=', 'accept')
      //.whereBetween(date,["staff_leave.start_time","staff_leave.start_time"]); 
      console.log("🚀 ~ file: ManagementService.ts ~ line 66 ~ ManagementService ~ leaveRecord ~ leaveRecord", leaveRecord)


      return leaveRecord;

   }

   async getStaff_Department() {
      const getStaff_Department = await this.knex.from(tables.staff)
         .select('username', 'department_name', "staff.id")
         .leftJoin(tables.department, "department.id", "staff.department_id")
         .from(tables.staff)
      console.log("🚀 ~ file: ManagementService.ts ~ line 65 ~ ManagementService ~ loadDepartment ~ loadDepartment", getStaff_Department)

      return getStaff_Department;
   }

   async getStaffLateRecord(staffId: number) {

      const getStaffLateRecord = await this.knex.from(tables.staff_registration)
         .select('checkin_time')
         .from(tables.staff_registration)
         .whereRaw(`staff_id = ${staffId}`)
         .andWhereRaw(`checkin_time::time > '09:00:00'`);//!! raw sql, select * from "TimeColumn" where "start"::TIME at time zone 'Time zone name' > 09:00:00
      console.log("🚀 ~ file: ManagementService.ts ~ line 97 ~ ManagementService ~ getStaffLateRecord ~ getStaffLateRecord", getStaffLateRecord)

      return getStaffLateRecord;
   }

   async getSIA(staffId: number) {
      const getSIA = await this.knex.from(tables.staff)
         .select('username', 'department_name', ' authority_level', 'salary')
         .leftJoin(tables.department, "department.id", "staff.department_id")
         .leftJoin(tables.staff_salary, 'staff_salary.staff_id', "staff.id")
         .where('staff.id', '=', staffId);
      console.log("🚀 ~ file: ManagementService.ts ~ line 107 ~ ManagementService ~ getSIA ~ getSIA", getSIA)
      return getSIA;
   }

   async adjustAuthorityLevel(updateLv: number, staffId: number) {
      await this.knex.from(tables.staff)
         .where('staff.id', '=', staffId)
         .update(
            {
               authority_level: updateLv
            }
         )
   }

   async adjustSalary(adjustSalary: number, staffId: number) {
      await this.knex.from(tables.staff_salary)
         .where('staff_salary.staff_id', '=', staffId)
         .update(
            {
               salary: adjustSalary
            }
         )

   }

   async getLeaveApply() {
      const staffLeave = await this.knex.from(tables.staff)
         .select('username', 'department_name', 'staff_leave.start_time', 'staff_leave.end_time', 'vacation_name', 'staff_leave.id')
         .leftJoin(tables.staff_leave, "staff_leave.staff_id", "staff.id")
         .leftJoin(tables.department, "department.id", "staff.department_id")
         .leftJoin(tables.vacation_type, 'staff_leave.leave_type', 'vacation_type.id')
         .where('staff_leave.status', '=', 'apply')
      console.log("🚀 ~ file: ManagementService.ts ~ line 142 ~ ManagementService ~ getLeaveApply ~ staffLeave", staffLeave)
      return staffLeave;
   }

   async setLeaveApplyResult(result: string, applyId: number) {
      await this.knex.from(tables.staff_leave)
         .where('staff_leave.id', '=', applyId)
         .update(
            {
               status: result
            }
         )
   }

   async getStaffLeaveRecord(staffId: number){
     const getStaffLeaveRecord =  await this.knex.from(tables.staff_leave)
      .select('start_time','end_time','vacation_name')
      .leftJoin(tables.vacation_type, 'staff_leave.leave_type', 'vacation_type.id')
      .where('staff_leave.staff_id','=',staffId)
      .andWhere('staff_leave.status', '=', 'accept')
      console.log("🚀 ~ file: ManagementService.ts ~ line 145 ~ ManagementService ~ getStaffLeaveRecord ~ getStaffLeaveRecord", getStaffLeaveRecord)

      return getStaffLeaveRecord;
   }

}