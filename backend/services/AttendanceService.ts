import Knex from 'knex';
import tables from './tables';

export class AttendanceService {
    public constructor(private knex: Knex) {}

    public heartBeat = async (userId: number, time: string) => {
        const clickHeartBeat = await this.knex(tables.heart_beat).insert({
            staff_id: userId,
            heatBeat: true,
            heartBeat_time: time,
        });

        return clickHeartBeat;
    };

    public lastHeartBeatTime = async (userId: number) => {
        const lastHeartBeatTime = await this.knex(tables.heart_beat)
            .select('heartBeat_time')
            .where('staff_id', '=', userId)
            .orderBy('heartBeat_time', 'desc')
            .first();
        return lastHeartBeatTime;
    };

    public getLastClockIn = async (userId: number) => {
        const loading = await this.knex(tables.staff_registration)
            .select('checkin_time', 'checkout_time', 'id', 'on_working')
            .where('staff_id', '=', userId)
            .orderBy('checkin_time', 'desc')
            .first();

        return loading;
    };

    public lastHTime = async (userId: number) => {
        const lastHeartBeatTime = await this.knex(tables.heart_beat)
            .select('heartBeat_time')
            .where('staff_id', '=', userId)
            .orderBy('heartBeat_time', 'desc')
            .first();
        return lastHeartBeatTime;
    };

    public getStaffAttendance = async (userId: number) => {
        const getStaffAttendance = await this.knex(tables.staff_registration)
            .select('checkin_time', 'checkout_time')
            .where('staff_id', '=', userId)
            .orderBy('checkin_time', 'desc')
            .limit(5);

        return getStaffAttendance;
    };

    public getAttendanceRangeData = async (userId: number, startDate: string, endDate: string) => {
        const getStaffAttendance = await this.knex(tables.staff_registration)
            .select('checkin_time', 'checkout_time')
            .where('staff_id', '=', userId)
            .andWhere('checkin_time', '>=', startDate)
            .andWhere('checkout_time', '<=', endDate);

        return getStaffAttendance;
    };

    public getHeartBeatData = async () => {
        const getHeartBeatData = await this.knex(tables.heart_beat)
            .select(this.knex.raw('max("heartBeat_time") as "heartBeat_time"'), this.knex.raw('max("checkin_time") as "checkin_time"'), 'username')
            .join(tables.staff_registration, 'staff_registration.staff_id', 'heart_beat.staff_id')
            .leftJoin(tables.staff, 'staff.id', 'heart_beat.staff_id')
            .where('on_working', '=', true)

            .groupBy('heart_beat.staff_id', 'username')
            .limit(10);
        // .select(this.knex.raw('select heart_beat from heartBeat_time order by heartBeat_time DESC liit 2'), 'username', 'checkin_time', 'checkout_time')
        // .join(tables.staff_registration, 'staff_registration.staff_id', 'heart_beat.staff_id')
        // .leftJoin(tables.staff, 'staff.id', 'heart_beat.staff_id')
        // .where('on_working', '=', true)
        // .orderBy('heartBeat_time', 'desc')
        // .limit(10);
        console.log('getheartBeatData:  ', getHeartBeatData);
        return getHeartBeatData;
    };
}
