import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import Knex from 'knex';
import cors from 'cors';
import path from 'path';

import { UserController } from './controllers/UserController';
import { UserService } from './services/UserService';
import { ClockInController } from './controllers/ClockInController';
import { ClockInService } from './services/ClockInService';

import { VacationService } from './services/VacationService';
import { VacationController } from './controllers/VacationController';
import { OtService } from './services/OtService';
import { OtController } from './controllers/OtController';
import { AttendanceService } from './services/AttendanceService';
import { AttendanceController } from './controllers/AttendanceController';

import { HomePageService } from './services/HomePageService';
import { HomePageController } from './controllers/HomePageController';
import { ManagementService } from './services/ManagementService';
import { ManagementController } from './controllers/ManagementController';

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
const API_VERSION = '/api/v1';

dotenv.config();

const app = express();
console.log(process.env.REACT_HOST!);

app.use(
    cors({
        origin: [process.env.REACT_HOST!],
    })
);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

export const userService = new UserService(knex);
export const clockInService = new ClockInService(knex);
export const vacationService = new VacationService(knex);
export const managementService = new ManagementService(knex);
export const otService = new OtService(knex);
export const homePageService = new HomePageService(knex);
export const attendanceService = new AttendanceService(knex);

export const userController = new UserController(userService);
export const clockInController = new ClockInController(clockInService);
export const vacationController = new VacationController(vacationService);
export const managementController = new ManagementController(managementService);

export const otController = new OtController(otService);
export const homePageController = new HomePageController(homePageService);
export const attendanceController = new AttendanceController(attendanceService);

import routes from './routes';

app.use(API_VERSION, routes);

app.use(express.static('../ui/build'));
app.use((req, res) => {
    res.sendFile(path.resolve('../ui/build/index.html'));
});

const port = 8500;
app.listen(port, () => {
    console.log('Listening on port ' + port);
});
