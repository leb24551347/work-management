// import { Request, Response, NextFunction } from 'express';

// export function isLoggedInAdmin(req: Request, res: Response, next: NextFunction) {
//     if (req.session?.['user'].is_admin) {
//         next();
//     } else {
//         console.log('isLoggedInAdmin: he is not admin');
//     }
// }

// export function isLoggedInHTML(req: Request, res: Response, next: NextFunction) {
//     if (req.session?.['user']) {
//         next();
//     } else {
//         console.log('isLoggedInHTML: he is not login');
//     }
// }

// export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
//     if (req.session?.['user']) {
//         next();
//     } else {
//         res.status(401).json({ message: 'You must be logged in' });
//         return;
//     }
// }

import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';
import { User } from '../common/types';
import { userService } from '././main';

const permit = new Bearer({
    query: 'access_token',
});

export async function isLoggedIn(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        const token = permit.check(req);
        // console.log('guards token: ', token);
        if (!token) {
            return res.status(401).json({ msg: 'Permission Denied' });
        }
        const payload = jwtSimple.decode(token, jwt.jwtSecret);
        const user: User = await userService.getUser(payload.id);
        // console.log('guards user: ', user.id);
        // console.log('guards payload: ', payload);

        if (user) {
            req.user = user;
            return next();
        } else {
            return res.status(401).json({ msg: 'Permission Denied' });
        }
    } catch (e) {
        return res.status(401).json({ msg: 'Permission Denied' });
    }
}
