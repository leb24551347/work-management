import type { Request, Response } from 'express';
import type { UserService } from '../services/UserService';
// import { checkPassword } from '../hash';
import type express from 'express';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';

export class UserController {
    public constructor(private userService: UserService) {}

    // public getSelfInfo = async (req: Request, res: Response) => {
    //     try {
    //         const userID = req.session?.['user'].id;
    //         const user = await this.userService.getUserInfo(userID);
    //         if (user) {
    //             const { password, ...others } = user;
    //             res.json({ user: others });
    //             return;
    //         }
    //         res.json({ message: 'cannot found user' }); // impossible
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // };

    public getUser = async (req: Request, res: Response) => {
        console.log('this is get user rotues');

        res.json(req.user);
    };

    public ChangePassword = async (req: Request, res: Response) => {
        console.log('this is /users/changePassword route');

        try {
            const { password, newPassword } = req.body;

            const result = await this.userService.changePassword(req.user.id, password, newPassword);

            if (!result) {
                res.status(400).json({
                    result: false,
                    message: 'incorrect username or password',
                });
                return;
            }
            res.json({
                result: true,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public login = async (req: express.Request, res: express.Response) => {
        console.log('this is /users/login route');

        try {
            const { username, password } = req.body;
            // Step 0: check if email / password empty
            // Step 1: get User by username
            const user = await this.userService.getUserByUserName(username);
            console.log(user);

            if (!user) {
                // Step 2: check if User exist, if not exist, collect skin

                res.status(400).json({
                    result: false,
                    message: 'incorrect username or password',
                });
                return;
            }
            // Step 3: if user exist, check password
            if (!(await this.userService.checkPassword(user.id, password))) {
                // password not match

                res.status(400).json({
                    result: false,
                    message: 'incorrect username or password',
                });
                return;
            }

            const payload = {
                id: user.id,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            if (req.session) {
                req.session['user'] = {
                    id: user.id,
                };
            }
            res.json({
                message: 'success',
                result: true,
                user: user,
                token: token,
                authority_level: user.authority_level,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public register = async (req: express.Request, res: express.Response) => {
        try {
            console.log('this is register route');
            const { username, password, departmentId, authorityLevel, staffSalary } = req.body;

            //TODO:
            console.log(staffSalary);

            const user = await this.userService.getUserByUserName(username);
            if (user) {
                res.status(400).json({
                    message: 'duplicated user',
                });
                return;
            }

            const userID = await this.userService.createUser(username, password, departmentId, authorityLevel);

            if (req.session) {
                req.session['user'] = {
                    id: userID,
                };
            }

            res.json({
                message: 'create user success',
                user_id: userID,
                result: true,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public department = async (req: Request, res: Response) => {
        console.log('this is department route');
        try {
            const rows = await this.userService.department();
            res.json(rows);
        } catch (err) {
            console.error(err.message);
        }
    };
}
