import type { Request, Response } from 'express';
import type { ClockInService } from '../services/ClockInService';

export class ClockInController {
    public constructor(private clockInService: ClockInService) {}

    public loadClockData = async (req: Request, res: Response) => {
        console.log('this is load data route');
        try {
            const lastClockIn = await this.clockInService.getLastClockIn(req.user.id);
            console.log('load: ', lastClockIn);

            if (lastClockIn.checkout_time != null) {
                res.json({
                    statusMsg: 'Out Time',
                    timer: lastClockIn.checkout_time,
                    id: lastClockIn.id,
                    on_working: lastClockIn.on_working,
                    result: true,
                });
                console.log('show check out time');
            } else {
                res.json({
                    statusMsg: 'In Time',
                    timer: lastClockIn.checkin_time,
                    id: lastClockIn.id,
                    on_working: lastClockIn.on_working,
                    result: true,
                });
                console.log('show check in time');
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public clockIn = async (req: Request, res: Response) => {
        console.log('this is clock in route');
        try {
            const time = new Date().toISOString();
            console.log('send clock in time: ', time);

            const lastClockIn = await this.clockInService.getLastClockIn(req.user.id);
            console.log('lastClockIn control:   ', lastClockIn);

            if (!lastClockIn || lastClockIn.checkout_time != null) {
                await this.clockInService.clockIn(time, req.user.id);
                console.log('normal clock in');
                res.json({
                    // id: check.id,
                    checkin_time: time,
                    result: true,
                    status: 'normal',
                });

                return;
            }
            res.json({
                result: false,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public clockOut = async (req: Request, res: Response) => {
        console.log('this is clock out route', req.body);

        try {
            const time = new Date().toISOString();

            const loadData = await this.clockInService.loadClockData(req.user.id);
            console.log('load:  ', loadData.id);

            if (loadData.checkout_time !== null) {
                res.json({
                    result: false,
                });
                return;
            } else {
                await this.clockInService.clockOut(time, req.user.id, loadData.id);

                res.json({
                    result: true,
                });
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };
}
