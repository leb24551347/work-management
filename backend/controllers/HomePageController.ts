import { HomePageService } from '../services/HomePageService';
import type { Request, Response} from 'express';


export class HomePageController{
    public constructor(public homePageService: HomePageService) {}

    public loadInfo = async (req:Request, res: Response)=>{
        const  start = req.query.month;
        console.log("🚀 ~ file: HomePageController.ts ~ line 10 ~ HomePageController ~ loadInfo= ~ start", start)

        console.log('this is homepage route');
        try {
            console.log("controller");
            console.log(req.user.id);
            
            const employeeDetail = await this.homePageService.getEmployeeDetail(req.user.id, start as string);
            console.log(employeeDetail);
            
            res.json(employeeDetail);
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }

        
    };
    public getMonthInfo = async (req: Request, res: Response) => {
            console.log('this is info for the selected month');
            // const { start, end } = req.query;
            try {
                // const staffMonthData = await this.homePageService.getMonthInfo(req.user.id, start as string, end as string);
                // res.json({
                //     staffMonthData,
                // });
            } catch (err) {
                console.error(err.message);
            }

    // public selectMonth = async (req:Request, res: Response)=>{
    //     // console.log("🚀 ~ file: HomePageController.ts ~ line 27 ~ HomePageController ~ sendMonth= ~ req.body", req.body, req.user.id)
    //     try {
    //         const selectedMonth = await this.homePageService.selectMonth(req.user.id, req.body.selectedMonth);
    //         res.json(selectedMonth);
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }


    // };
    }

}