import type { Request, Response } from 'express';
import { AttendanceService } from '../services/AttendanceService';

export class AttendanceController {
    public constructor(private attendanceService: AttendanceService) {}

    public heartBeat_time = async (req: Request, res: Response) => {
        console.log('this is get heart beat time route');

        try {
            const lastHeartBeatTime = await this.attendanceService.lastHeartBeatTime(req.user.id);
            const getLastClockIn = await this.attendanceService.getLastClockIn(req.user.id);

            console.log('lastHeartBeatTime: ', lastHeartBeatTime);
            console.log('getLastClockIn: ', getLastClockIn);

            if (!getLastClockIn) {
                res.json({
                    result: false,
                });
                return;
            }
            if (getLastClockIn.checkout_time != null) {
                res.json({
                    result: false,
                });
                return;
            }
            if (!lastHeartBeatTime) {
                res.json({
                    result: true,
                    time: getLastClockIn.checkin_time,
                });
                return;
            }

            if (lastHeartBeatTime.heartBeat_time > getLastClockIn.checkin_time) {
                res.json({
                    result: true,
                    time: lastHeartBeatTime.heartBeat_time,
                });
            } else {
                res.json({
                    result: true,
                    time: getLastClockIn.checkin_time,
                });
            }
        } catch (err) {
            console.error(err.message);

            res.status(500).json({
                result: false,
                message: 'internal server error',
            });
        }
    };

    public heartBeat = async (req: Request, res: Response) => {
        console.log('this is heat beat route');
        const time = new Date().toISOString();
        try {
            await this.attendanceService.heartBeat(req.user.id, time);

            res.json({
                result: true,
                lastHeartBeatTime: time,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public getStaffAttendance = async (req: Request, res: Response) => {
        console.log('this is get Staff Attendance route');

        try {
            const getStaffAttendance = await this.attendanceService.getStaffAttendance(req.user.id);
            // console.log(getStaffAttendance);
            res.json({
                getStaffAttendance,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public getAttendanceRangeData = async (req: Request, res: Response) => {
        console.log('this is get Staff Attendance range data route');

        const { start, end } = req.query;

        try {
            const getAttendanceRangeData = await this.attendanceService.getAttendanceRangeData(req.user.id, start as string, end as string);
            console.log(getAttendanceRangeData, 'getAttendanceRangeData');
            res.json({
                getAttendanceRangeData,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    public getHeartBeatData = async (req: Request, res: Response) => {
        console.log('this is get heat beat route');

        try {
            const heartBeatData = await this.attendanceService.getHeartBeatData();

            res.json({
                result: true,
                heartBeatData: heartBeatData,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };
}
