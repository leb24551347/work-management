import { OtService } from '../services/OtService';
import type { Request, Response } from 'express';

export class OtController {
    public constructor(private otService: OtService) {}

    public applyOt = async (req: Request, res: Response) => {
        console.log('this is apply ot route');
        try {
            const { otStartDay, otEndDay, otReason } = req.body;
            if (otEndDay < otStartDay || otStartDay === otEndDay) {
                res.json({
                    small: true,
                });
                return;
            }

            const otData = await this.otService.getOtStatus(req.user.id);
            for (const otDataKey of otData) {
                console.log(otDataKey.start_time.toISOString(), 'test.start_time');
                if (otDataKey.start_time.toISOString() === otStartDay && otDataKey.end_time.toISOString() === otEndDay) {
                    console.log('ot multiple application');
                    res.json({
                        double: true,
                    });
                    return;
                }
            }

            const checkOT = await this.otService.checkOt(req.user.id, otStartDay, otEndDay, otReason);

            if (checkOT.length === 0) {
                await this.otService.applyOt(req.user.id, otStartDay, otEndDay, otReason);

                res.json({
                    result: true,
                });
            } else {
                res.json({
                    result: false,
                });
            }
        } catch (err) {
            console.error(err.message);
        }
    };

    // public getOtStatus = async (req: Request, res: Response) => {
    //     console.log('this is load ot status route');
    //     try {
    //         const otData = await this.otService.getOtStatus(req.user.id);
    //         res.json({
    //             otData,
    //         });
    //     } catch (err) {
    //         console.error(err.message);
    //     }
    // };

    public getOtData = async (req: Request, res: Response) => {
        console.log('this is Vacation Data route');
        const { start, end } = req.query;
        try {
            const staffOtData = await this.otService.getOtData(req.user.id, start as string, end as string);
            res.json({
                staffOtData,
            });
        } catch (err) {
            console.error(err.message);
        }
    };
}
