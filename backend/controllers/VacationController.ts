import type { Request, Response } from 'express';
import type { VacationService } from '../services/VacationService';

export class VacationController {
    public constructor(private VacationService: VacationService) {}

    public vacation = async (req: Request, res: Response) => {
        console.log('this is vacation route');
        try {
            const rows = await this.VacationService.vacation();
            res.json(rows);
        } catch (err) {
            console.error(err.message);
        }
    };

    public applyVacation = async (req: Request, res: Response) => {
        console.log('this is apply Vacation route');
        try {
            const { vacationId, startDay, endDay } = req.body;

            if (endDay < startDay || startDay === endDay) {
                res.json({
                    small: true,
                });
                return;
            }
            const vacationData = await this.VacationService.getVacationStatus(req.user.id);

            for (const vacationDataKey of vacationData) {
                console.log(vacationDataKey.start_time.toISOString(), 'test.start_time');
                if (vacationDataKey.start_time.toISOString() === startDay && vacationDataKey.end_time.toISOString() === endDay) {
                    console.log('vacation double apply');
                    res.json({
                        double: true,
                    });
                    return;
                }
            }
            // console.log('vacationId', vacationId);
            // console.log('startDay', startDay);
            // console.log('endDay', endDay);

            const checkingDate = await this.VacationService.checkApplyVacation(req.user.id, vacationId, startDay, endDay);
            // console.log('checkingDate', checkingDate.length);
            if (checkingDate.length === 0) {
                await this.VacationService.applyVacation(req.user.id, vacationId, startDay, endDay);
                res.json({
                    result: true,
                });
            } else {
                res.json({
                    result: false,
                });
            }
        } catch (err) {
            console.error(err.message);
        }
    };

    // public getVacationStatus = async (req: Request, res: Response) => {
    //     console.log('this is load vacation status route');
    //     try {
    //         const vacationData = await this.VacationService.getVacationStatus(req.user.id);
    //         res.json({
    //             vacationData,
    //         });
    //     } catch (err) {
    //         console.error(err.message);
    //     }
    // };
    public getVacationData = async (req: Request, res: Response) => {
        console.log('this is secify Apply Vacation route');
        const { start, end } = req.query;

        try {
            const secifyApplyVacation = await this.VacationService.getVacationData(req.user.id, start as string, end as string);
            res.json({
                secifyApplyVacation,
            });
        } catch (err) {
            console.error(err.message);
        }
    };
}
