import type { Request, Response } from 'express';
import type { ManagementService } from '../services/ManagementService';


export class ManagementController {
    public constructor(private managementService: ManagementService) { }

    public onTimeRecord = async (req: Request, res: Response) => {
        // console.log(req.body);
        // console.log(req.body.selectedDate);
        console.log("🚀 ~ file: ManagementController.ts ~ line 14 ~ ManagementController ~ onTimeRecord= ~ req.body.selectedDate", req.body.selectedDate)
        try {

            const result = await this.managementService.onTimeRecord(req.body.selectedDate);
            // console.log("Controller");    
            // console.log(result);

            res.json(
                result
            )

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    public lateRecord = async (req: Request, res: Response) => {
        console.log("🚀 ~ file: ManagementController.ts ~ line 29 ~ ManagementController ~ lateRecord= ~ req.body.selectedDate", req.body.selectedDate)
        try {

            const result = await this.managementService.lateRecord(req.body.selectedDate);
            // console.log("Controller");    
            // console.log(result);

            res.json(
                result
            )

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }

    }

    public leaveRecord = async (req: Request, res: Response) => {
        console.log("🚀 ~ file: ManagementController.ts ~ line 48 ~ ManagementController ~ lateRecord= ~ req.body.selectedDate", req.body.selectedDate)
        try {

            const result = await this.managementService.leaveRecord(req.body.selectedDate);
            // console.log("Controller");    
            // console.log(result);

            res.json(
                result
            )

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }

    }

    public getStaff_Department = async (req: Request, res: Response) => {

        try {
            const result = await this.managementService.getStaff_Department();

            res.json(
                result
            )

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }


    public getStaffLateRecord = async (req: Request, res: Response) => {
        console.log("🚀 ~ file: ManagementController.ts ~ line 83 ~ ManagementController ~ getStaffLateRecord= ~ req.body.staffId", req.body.staffId)

        try {
            const result = await this.managementService.getStaffLateRecord(req.body.staffId);
            res.json(
                result
            )
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }


    public getSIA = async (req: Request, res: Response) => {
        console.log("🚀 ~ file: ManagementController.ts ~ line 110 ~ ManagementController ~ getSIA= ~ req.body.staffId", req.body.staffId)

        try {
            const result = await this.managementService.getSIA(req.body.staffId);
            res.json(
                result
            )
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    public adjustAuthorityLevel = async (req: Request, res: Response) => {
        console.log("🚀 ~ file: ManagementController.ts ~ line 121 ~ ManagementController ~ adjustAuthorityLevel= ~ req.body.updateLv", req.body.updateLv)

        try {
            if (req.body.updateLv > 0 && req.body.updateLv <= 5) {
                const result = await this.managementService.adjustAuthorityLevel(req.body.updateLv,req.body.staffId)
                res.json(
                    result
                )
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }

    }

    public adjustSalary = async (req: Request, res: Response) => {
        try {
            const result  = await this.managementService.adjustSalary(req.body.updateSalary,req.body.staffId);
            res.json(
                result
            )
        }catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    public getLeaveApply = async(req: Request, res: Response)=>{
        try{
            const result = await this.managementService.getLeaveApply();
            console.log("🚀 ~ file: ManagementController.ts ~ line 143 ~ ManagementController ~ getLeaveApply=async ~ result", result)            
            res.json(
                result
            )
        }catch(err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    public setLeaveApplyResult = async(req: Request, res: Response)=>{
        console.log("🚀 ~ file: ManagementController.ts ~ line 168 ~ req.body.result", req.body.result,req.body.leaveApplyId)
        try{
            const result = await this.managementService.setLeaveApplyResult(req.body.result,req.body.leaveApplyId);
            console.log("🚀 ~ file: ManagementController.ts ~ line 143 ~ ManagementController ~ getLeaveApply=async ~ result", result)
        }catch(err){
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    public getStaffLeaveRecord = async(req: Request, res: Response)=>{
        try{
            const result = await this.managementService.getStaffLeaveRecord(req.body.staffId);
            res.json(
                result
            )
        }catch(err){
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
}

