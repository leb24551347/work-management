import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_ot', (table) => {
        table.dropColumn('ot_hours');
        table.dropColumn('ot_date');
    });
    await knex.schema.alterTable('staff_ot', (table) => {
        table.dateTime('start_time');
        table.dateTime('end_time');
        table.string('status');
        table.string('reason');
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_ot', (table) => {
        table.dropColumn('start_time');
        table.dropColumn('end_time');
        table.dropColumn('status');
        table.dropColumn('reason');
    });
    await knex.schema.alterTable('staff_ot', (table) => {
        table.string('ot_hours');
        table.string('ot_date');
    });
}
