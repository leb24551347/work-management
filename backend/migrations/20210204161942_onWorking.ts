import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_registration', (table) => {
        table.string('on_working');
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_registration', (table) => {
        table.dropColumn('on_working');
    });
}
