import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.dropTable('heat_beat');

    await knex.schema.createTable('heart_beat', (table) => {
        table.increments();
        table.boolean('heatBeat');
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.dateTime('heartBeat_time');
        table.timestamps(false, true);
    });
}
export async function down(knex: Knex): Promise<void> {
    await knex.schema.createTable('heat_beat', (table) => {
        table.increments();
        table.boolean('heatBeat');
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.timestamps(false, true);
    });

    await knex.schema.dropTable('heart_beat');
}
