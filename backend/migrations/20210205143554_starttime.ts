import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_leave', (table) => {
        table.string('start_time');
        table.string('status');
    });
    await knex.schema.alterTable('staff_leave', (table) => {
        table.renameColumn('date', 'end_time');
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_leave', (table) => {
        table.renameColumn('end_time', 'date');
    });
    await knex.schema.alterTable('staff_leave', (table) => {
        table.dropColumn('start_time');
        table.dropColumn('status');
    });
}
