import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_leave', (table) => {
        table.dropColumn('start_time');
        table.dropColumn('end_time');
    });
    await knex.schema.alterTable('staff_leave', (table) => {
        table.dateTime('start_time');
        table.dateTime('end_time');
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_leave', (table) => {
        table.dropColumn('start_time');
        table.dropColumn('end_time');
    });
    await knex.schema.alterTable('staff_leave', (table) => {
        table.string('start_time');
        table.string('end_time');
    });
}
