import * as Knex from 'knex';

//1
export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('department', (table) => {
        table.increments();
        table.string('department_name');
        table.timestamps(false, true);
    });

    //2
    await knex.schema.createTable('staff', (table) => {
        table.increments();
        table.string('username').notNullable().unique();
        table.string('password').notNullable();
        table.integer('authority_level').defaultTo(1);
        table.integer('department_id');
        table.foreign('department_id').references('department.id');
        table.timestamps(false, true);
    });

    //3
    await knex.schema.createTable('staff_registration', (table) => {
        table.increments();
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.dateTime('checkin_time');
        table.dateTime('checkout_time');
        table.timestamps(false, true);
    });

    //4
    await knex.schema.createTable('ot_allowance_level', (table) => {
        table.increments();
        table.integer('allowance');
        table.timestamps(false, true);
    });

    //5
    await knex.schema.createTable('staff_performance', (table) => {
        table.increments();
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.timestamps(false, true);
    });

    //6
    await knex.schema.createTable('staff_salary', (table) => {
        table.increments();
        table.integer('salary');
        table.integer('bonus');
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.date('date');
        table.timestamps(false, true);
    });

    //7
    await knex.schema.createTable('staff_leave', (table) => {
        table.increments();
        table.string('leave_type');
        table.date('date');
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.timestamps(false, true);
    });

    //8
    await knex.schema.createTable('staff_ot', (table) => {
        table.increments();
        table.integer('staff_id');
        table.foreign('staff_id').references('staff.id');
        table.integer('ot_hours');
        table.date('ot_date');
        table.integer('allowance_id');
        table.foreign('allowance_id').references('ot_allowance_level.id');
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('staff_ot');
    await knex.schema.dropTable('staff_leave');
    await knex.schema.dropTable('staff_salary');
    await knex.schema.dropTable('staff_performance');
    await knex.schema.dropTable('ot_allowance_level');
    await knex.schema.dropTable('staff_registration');
    await knex.schema.dropTable('staff');
    await knex.schema.dropTable('department');
}
