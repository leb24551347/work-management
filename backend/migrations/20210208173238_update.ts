import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_leave', (table) => {
        table.dropColumn('leave_type');
    });
    await knex.schema.alterTable('staff_leave', (table) => {
        table.integer('leave_type').references('vacation_type.id');
    });
}
export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('staff_leave', (table) => {
        table.dropColumn('leave_type');
    });
    await knex.schema.alterTable('staff_leave', (table) => {
        table.string('leave_type');
    });
}
