import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("staff")){
        await knex.schema.table("staff",(table)=>{
            table.integer("staff_ot_id");
            table.foreign("staff_ot_id").references("staff_ot.id");
        });  
    }
};


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("staff")){
        await knex.schema.alterTable("staff",(table)=>{
            table.dropColumn("staff_ot_id");
        });
    }
}
