import * as Knex from 'knex';
import { hashPassword } from '../hash';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('staff_ot').del();
    await knex('staff_leave').del();
    await knex('staff_salary').del();
    await knex('staff_performance').del();
    await knex('ot_allowance_level').del();
    await knex('staff_registration').del();
    await knex('staff').del();
    await knex('department').del();
    await knex('vacation_type').del();

    //Inserts seed entries
    const password = await hashPassword('12345');

    const vacationTypeIds = await knex('vacation_type')
        .insert([
            { vacation_name: 'Sick Leave' },
            { vacation_name: 'Annual Leave' },
            { vacation_name: 'Casual Leave' },
            { vacation_name: 'Marriage Leave' },
            { vacation_name: 'Maternity Leave' },
            { vacation_name: 'Paternity Leave' },
            { vacation_name: 'Jury Service Leave' },
            { vacation_name: 'Compassionate Leave' },
        ])
        .returning('id');

    const departmentIds = await knex('department')
        .insert([{ department_name: 'Marketing' }, { department_name: 'HR' }, { department_name: 'IT' }, { department_name: 'Admin' }])
        .returning('id');

    const staff_id: number[] = await knex('staff')
        .insert([
            { username: 'Andy', password: password, department_id: departmentIds[0], authority_level: 5 },
            { username: 'Billy', password: password, department_id: departmentIds[2], authority_level: 4 },
            { username: 'Charlie', password: password, department_id: departmentIds[3] },
            { username: 'Daniel', password: password, department_id: departmentIds[2] },
            { username: 'Elise', password: password, department_id: departmentIds[1] },

            { username: 'Franco', password: password, department_id: departmentIds[1] },
            { username: 'George', password: password, department_id: departmentIds[3] },
            { username: 'Harry', password: password, department_id: departmentIds[1] },
            { username: 'Isabel', password: password, department_id: departmentIds[1] },
            { username: 'Joe', password: password, department_id: departmentIds[3], authority_level: 1 },

            { username: 'Kelly', password: password, department_id: departmentIds[2] },
        ])
        .returning('id');

    await knex('staff_registration').insert([
        { staff_id: staff_id[0], checkin_time: '2021-01-04 08:40:00+08', checkout_time: '2021-01-04 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-05 08:41:00+08', checkout_time: '2021-01-05 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-06 08:42:00+08', checkout_time: '2021-01-06 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-07 08:43:00+08', checkout_time: '2021-01-07 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-08 08:46:00+08', checkout_time: '2021-01-08 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-11 08:50:00+08', checkout_time: '2021-01-11 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-12 08:52:00+08', checkout_time: '2021-01-12 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-13 08:53:00+08', checkout_time: '2021-01-13 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-14 08:55:00+08', checkout_time: '2021-01-14 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-15 08:58:00+08', checkout_time: '2021-01-15 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-18 08:50:00+08', checkout_time: '2021-01-18 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-19 08:58:00+08', checkout_time: '2021-01-19 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-20 08:55:00+08', checkout_time: '2021-01-20 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-21 08:52:00+08', checkout_time: '2021-01-21 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-22 09:00:00+08', checkout_time: '2021-01-22 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-25 09:10:00+08', checkout_time: '2021-01-25 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-26 09:11:00+08', checkout_time: '2021-01-26 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-27 09:13:00+08', checkout_time: '2021-01-27 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-28 09:15:00+08', checkout_time: '2021-01-28 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-01-29 09:12:00+08', checkout_time: '2021-01-29 18:00:40+08', on_working: false },

        { staff_id: staff_id[0], checkin_time: '2021-02-01 08:55:30+08', checkout_time: '2021-02-01 18:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-02 08:50:30+08', checkout_time: '2021-02-02 19:00:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-03 08:58:30+08', checkout_time: '2021-02-03 18:10:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-04 09:00:30+08', checkout_time: '2021-02-04 18:12:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-05 09:15:30+08', checkout_time: '2021-02-05 18:11:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-08 09:05:30+08', checkout_time: '2021-02-08 18:09:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-09 08:58:30+08', checkout_time: '2021-02-09 18:05:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-10 08:56:30+08', checkout_time: '2021-02-10 18:03:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-11 09:11:30+08', checkout_time: '2021-02-11 18:18:40+08', on_working: false },
        { staff_id: staff_id[0], checkin_time: '2021-02-12 09:02:30+08', checkout_time: '2021-02-12 18:20:40+08', on_working: false },

        { staff_id: staff_id[9], checkin_time: '2021-01-04 08:40:00+08', checkout_time: '2021-01-04 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-05 08:41:00+08', checkout_time: '2021-01-05 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-06 08:42:00+08', checkout_time: '2021-01-06 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-07 08:43:00+08', checkout_time: '2021-01-07 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-08 08:46:00+08', checkout_time: '2021-01-08 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-11 08:50:00+08', checkout_time: '2021-01-11 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-12 08:52:00+08', checkout_time: '2021-01-12 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-13 08:53:00+08', checkout_time: '2021-01-13 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-14 08:55:00+08', checkout_time: '2021-01-14 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-15 08:58:00+08', checkout_time: '2021-01-15 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-18 08:50:00+08', checkout_time: '2021-01-18 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-19 08:58:00+08', checkout_time: '2021-01-19 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-20 08:55:00+08', checkout_time: '2021-01-20 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-21 08:52:00+08', checkout_time: '2021-01-21 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-22 09:00:00+08', checkout_time: '2021-01-22 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-25 09:10:00+08', checkout_time: '2021-01-25 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-26 09:11:00+08', checkout_time: '2021-01-26 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-27 09:13:00+08', checkout_time: '2021-01-27 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-28 09:15:00+08', checkout_time: '2021-01-28 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-01-29 09:12:00+08', checkout_time: '2021-01-29 20:00:40+08', on_working: false },

        { staff_id: staff_id[9], checkin_time: '2021-02-01 08:55:30+08', checkout_time: '2021-02-01 18:00:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-02 08:50:30+08', checkout_time: '2021-02-02 19:30:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-03 08:58:30+08', checkout_time: '2021-02-03 18:10:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-04 09:00:30+08', checkout_time: '2021-02-04 18:12:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-05 09:15:30+08', checkout_time: '2021-02-05 18:11:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-08 09:05:30+08', checkout_time: '2021-02-08 18:09:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-09 08:58:30+08', checkout_time: '2021-02-09 18:05:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-10 08:56:30+08', checkout_time: '2021-02-10 18:03:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-11 09:11:30+08', checkout_time: '2021-02-11 18:18:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-12 09:02:30+08', checkout_time: '2021-02-12 18:20:40+08', on_working: false },
        { staff_id: staff_id[9], checkin_time: '2021-02-12 09:02:30+08', checkout_time: '2021-02-12 18:20:40+08', on_working: false },

        { staff_id: staff_id[8], checkin_time: '2021-02-17 09:00:30+08', on_working: true },
        { staff_id: staff_id[7], checkin_time: '2021-02-17 09:30:30+08', on_working: true },
        { staff_id: staff_id[6], checkin_time: '2021-02-17 09:15:30+08', on_working: true },
        { staff_id: staff_id[5], checkin_time: '2021-02-17 09:20:30+08', on_working: true },
    ]);

    await knex('heart_beat').insert([
        { staff_id: staff_id[8], heartBeat_time: '2021-02-17 12:00:30+08' },
        { staff_id: staff_id[7], heartBeat_time: '2021-02-17 12:30:30+08' },
        { staff_id: staff_id[6], heartBeat_time: '2021-02-17 12:15:30+08' },
        { staff_id: staff_id[5], heartBeat_time: '2021-02-17 10:15:30+08' },
    ]);

    const allowanceIds = await knex('ot_allowance_level')
        .insert([{ allowance: 50 }, { allowance: 100 }, { allowance: 150 }, { allowance: 200 }, { allowance: 250 }])
        .returning('id');

    await knex('staff_performance').insert([
        { staff_id: staff_id[0] },
        { staff_id: staff_id[1] },
        { staff_id: staff_id[2] },
        { staff_id: staff_id[3] },
        { staff_id: staff_id[4] },
        { staff_id: staff_id[5] },
    ]);

    await knex('staff_salary').insert([
        { salary: 40000, bonus: 500, staff_id: staff_id[0], date: '2021-01-26' },
        { salary: 35000, bonus: 600, staff_id: staff_id[1], date: '2021-01-27' },
        { salary: 18000, bonus: 600, staff_id: staff_id[2], date: '2021-01-28' },
        { salary: 20000, bonus: 1000, staff_id: staff_id[3], date: '2021-01-29' },
        { salary: 23000, bonus: 1000, staff_id: staff_id[4], date: '2021-01-30' },

        { salary: 13000, bonus: 500, staff_id: staff_id[5], date: '2021-01-30' },
        { salary: 15000, bonus: 600, staff_id: staff_id[6], date: '2021-01-30' },
        { salary: 18000, bonus: 600, staff_id: staff_id[7], date: '2021-01-30' },
        { salary: 20000, bonus: 1000, staff_id: staff_id[8], date: '2021-01-30' },
        { salary: 20000, bonus: 1000, staff_id: staff_id[9], date: '2021-01-30' },

        { salary: 23000, bonus: 1000, staff_id: staff_id[10], date: '2021-01-30' },

        { salary: 40000, bonus: 600, staff_id: staff_id[9], date: '2021-02-26' },
        { salary: 40000, bonus: 700, staff_id: staff_id[9], date: '2021-03-26' },
        { salary: 40000, bonus: 800, staff_id: staff_id[9], date: '2021-04-26' },
        { salary: 40000, bonus: 900, staff_id: staff_id[9], date: '2021-05-26' },
    ]);
    ('2021-02-12 09:02:30+08');
    await knex('staff_leave').insert([
        {
            leave_type: vacationTypeIds[0],
            start_time: '2021-02-02 09:00:00+08',
            end_time: '2021-02-05 16:00:00+08',
            staff_id: staff_id[0],
            status: 'apply',
        },
        {
            leave_type: vacationTypeIds[1],
            start_time: '2021-02-06 09:00:00+08',
            end_time: '2021-02-10 16:00:00+08',
            staff_id: staff_id[9],
            status: 'apply',
        },
        {
            leave_type: vacationTypeIds[2],
            start_time: '2021-02-06 09:00:00+08',
            end_time: '2021-02-10 16:00:00+08',
            staff_id: staff_id[2],
            status: 'apply',
        },
        {
            leave_type: vacationTypeIds[3],
            start_time: '2021-02-06 09:00:00+08',
            end_time: '2021-02-10 16:00:00+08',
            staff_id: staff_id[3],
            status: 'apply',
        },
        {
            leave_type: vacationTypeIds[4],
            start_time: '2021-02-06 09:00:00+08',
            end_time: '2021-02-10 16:00:00+08',
            staff_id: staff_id[4],
            status: 'apply',
        },
        {
            leave_type: vacationTypeIds[1],
            start_time: '2021-02-06 09:00:00+08',
            end_time: '2021-02-10 16:00:00+08',
            staff_id: staff_id[5],
            status: 'apply',
        },
    ]);

    await knex('staff_ot').insert([
        {
            staff_id: staff_id[0],
            start_time: '2021-02-02 18:00:20+08',
            end_time: '2021-02-02 19:00:20+08',
            allowance_id: allowanceIds[3],
            reason: 'department meeting',
            status: 'apply',
        },
        {
            staff_id: staff_id[9],
            start_time: '2021-02-02 18:00:20+08',
            end_time: '2021-02-02 19:30:20+08',
            allowance_id: allowanceIds[1],
            reason: 'department meeting',
            status: 'apply',
        },
        {
            staff_id: staff_id[9],
            start_time: '2021-01-29 18:00:20+08',
            end_time: '2021-01-29 20:00:20+08',
            allowance_id: allowanceIds[1],
            reason: 'final project update',
            status: 'apply',
        },
        {
            staff_id: staff_id[2],
            start_time: '2021-02-02 18:00:20+08',
            end_time: '2021-02-02 20:00:20+08',
            allowance_id: allowanceIds[0],
            reason: 'department meeting',
            status: 'apply',
        },
    ]);
}
