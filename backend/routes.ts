import express from 'express';

import {
    userController,
    clockInController,
    vacationController,
    homePageController,
    otController,
    attendanceController,
    managementController,
} from './main';

import { isLoggedIn } from './guards';

const routes = express.Router();
routes.post('/users/login', userController.login);
routes.get('/user', isLoggedIn, userController.getUser);
routes.post('/users/register', isLoggedIn, userController.register);
routes.post('/changePassword', isLoggedIn, userController.ChangePassword);
routes.get('/department', isLoggedIn, userController.department);
routes.get('/vacation', isLoggedIn, vacationController.vacation);
routes.post('/applyVacation', isLoggedIn, vacationController.applyVacation);

routes.get('/getVacationData', isLoggedIn, vacationController.getVacationData);
routes.post('/clockIn', isLoggedIn, clockInController.clockIn);
routes.post('/clockOut', isLoggedIn, clockInController.clockOut);
routes.get('/loadClockData', isLoggedIn, clockInController.loadClockData);

routes.get('/EmployeeInfo', isLoggedIn, homePageController.loadInfo);

// routes.post('/sendMonth', isLoggedIn, homePageController.selectMonth);
routes.post('/applyOt', isLoggedIn, otController.applyOt);

routes.get('/getOtData', isLoggedIn, otController.getOtData);
routes.post('/heartBeat', isLoggedIn, attendanceController.heartBeat);
// routes.get('/heartBeat_time', isLoggedIn, attendanceController.heartBeat_time);
routes.get('/getHeartBeatData', isLoggedIn, attendanceController.getHeartBeatData);
routes.get('/getStaffAttendance', isLoggedIn, attendanceController.getStaffAttendance);
routes.get('/getAttendanceRangeData', isLoggedIn, attendanceController.getAttendanceRangeData);

routes.post('/onTimeRecord', isLoggedIn, managementController.onTimeRecord);
routes.post('/lateRecord', isLoggedIn, managementController.lateRecord);
routes.post('/leaveRecord', isLoggedIn, managementController.leaveRecord);
routes.get('/getStaff_Department', isLoggedIn, managementController.getStaff_Department);
routes.post('/getStaffLateRecord', isLoggedIn, managementController.getStaffLateRecord);
routes.post('/getSIA', isLoggedIn, managementController.getSIA);
routes.post('/adjustAuthorityLevel', isLoggedIn, managementController.adjustAuthorityLevel);
routes.post('/adjustSalary', isLoggedIn, managementController.adjustSalary);
routes.get('/getLeaveApply', isLoggedIn, managementController.getLeaveApply);
routes.post('/setLeaveApplyResult', isLoggedIn, managementController.setLeaveApplyResult);
routes.post('/getStaffLeaveRecord', isLoggedIn, managementController.getStaffLeaveRecord);

// if export default, no need to add {} in import
export default routes;
