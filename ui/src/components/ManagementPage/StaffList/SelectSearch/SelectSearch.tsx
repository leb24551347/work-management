import { StaffDetail } from 'common';
import React, { useEffect, useState } from 'react';
//import Select from 'react-select';
import { get } from '../../../../api';
import './SelectSearch.scss';
import { useDispatch } from 'react-redux';
//import { RootState } from '../../../../store';
import { SelectStaff } from '../../../../redux/ManagementPage/StaffList/action';

export function SelectSearch() {
    // let selectrfStaffId = useSelector((state: RootState) => state.StaffList.staffId)

    const dispatch = useDispatch();

    const [selectedDepartment, setSelectedDepartment] = useState("");
    const [selectedStaff, setSelectedStaff] = useState("");
    const [selectStaffId, setSelectStaffId] = useState<number>(0);

    const [Staff_Department, setStaff_Department] = useState<StaffDetail[]>([]);



    async function getStaff_Department() {
        const res = await get('/getStaff_Department');
        const json: StaffDetail[] = await res.json();
        setStaff_Department(json);
    };

    useEffect(
        () => {
            getStaff_Department();
        }, []
    )

    // useEffect(() => {
    //     console.log('is this repeating?')
    //     setSelectStaffId(1)
    //     console.log(selectStaffId)
    // )

    let departments = Staff_Department.map(u => u.department_name);
    const depSet = new Set(departments);
    departments = Array.from(depSet);

    let staff = Staff_Department.filter(u => selectedDepartment === "" || u.department_name === selectedDepartment).map(u => u.username);
    const staffSet = new Set(staff);
    staff = Array.from(staffSet);

    useEffect(
        () => {
            console.log("🚀 ~ file: SelectSearch.tsx ~ line 47 ~ SelectSearch ~ selectedStaff", selectedStaff)
            let staffIDTest = Staff_Department.filter(u => u.username === selectedStaff).map(u => u.id)[0];
            console.log("🚀 ~ file: SelectSearch.tsx ~ line 57 ~ SelectSearch ~ staffIDTest", staffIDTest)
            
            if (staffIDTest) {
                setSelectStaffId(staffIDTest);
                dispatch(SelectStaff(staffIDTest));

            }
        }, [selectedStaff, selectStaffId, Staff_Department, dispatch]
    );

    return (
        <>
            <select
                value={selectedDepartment}
                //options={data}
                onChange={e => setSelectedDepartment(e.target.value)}
            >
                <option value="">All Departments</option>
                {
                    departments.map(d => <option>{d}</option>)
                }

            </select>

            <select
                value={selectedStaff}
                //options={data}
                onChange={e => setSelectedStaff(e.target.value)}
            >
                <option value="">Staff List</option>

                {staff.map(s => <option>{s}</option>)}
            </select>

            {/* <br />
            <b>Selected Department:</b>
            <pre>{JSON.stringify(selectedDepartment, null, 2)}</pre>

            <br />
            <b>Selected Staff:</b>
            <pre>{JSON.stringify(selectedStaff, null, 2)}</pre> */}
        </>
    )
}
