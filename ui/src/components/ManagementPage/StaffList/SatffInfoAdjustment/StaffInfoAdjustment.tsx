import { StaffDetail } from 'common';
import React, { useEffect, useState } from 'react';
import { post } from '../../../../api';
import "./StaffInfoAdjustment.scss";
import Swal from 'sweetalert2';

export function StaffInfoAdjustment(
    props: {
        selectedStaffId: number
    }) {

    const [SIA, setSIA] = useState<StaffDetail[]>([]);
    const [adjustAuthorityLevel, setAdjustAuthorityLevel] = useState<number>();
    const [adjustSalary, setAdjustSalary] = useState<number>();


    

    useEffect(
        () => {
            async function updateSIA(staffId: number) {
                const res = await post('/getSIA', { 'staffId': staffId });
                const json = await res.json();
                setSIA(json);
            }
            
            updateSIA(props.selectedStaffId);
        }, [props.selectedStaffId,SIA]

    )



    return (
        <div className="staffInfoAdjustmentContainer">

            <div className="staffInfoAdjustmentBar">
                <div className="staffInfoAdjustmentStaffName">Staff Name</div>
                <div className="staffInfoAdjustmentDepartment">Department</div>
                <div className="staffInfoAdjustmentAuthorityLevel">Authority Level</div>
                <div className="staffInfoAdjustmentCurrentSalary">Current Salary</div>
            </div>

            <div className="staffInfoAdjustmentList">
                
                <div className="staffInfoAdjustmentListStaffName">
                    {
                        Object.keys(SIA).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {SIA[parseInt(itemKey)].username}</td>
                                </tr>
                            )
                        })
                    }
                </div>
                <div className="staffInfoAdjustmentListDepartment">
                    {
                        Object.keys(SIA).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {SIA[parseInt(itemKey)].department_name}</td>
                                </tr>
                            )
                        })
                    }
                </div>
                <div className="staffInfoAdjustmentListAuthorityLevel">
                    {
                        Object.keys(SIA).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {SIA[parseInt(itemKey)].authority_level}</td>
                                </tr>
                            )
                        })
                    }
                </div>
                <div className="staffInfoAdjustmentListCurrentSalary">
                    {
                        Object.keys(SIA).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {SIA[parseInt(itemKey)].salary}</td>
                                </tr>
                            )
                        })
                    }
                </div>
            </div>

            <div className="adjustmentColumn">
                <div></div>
                <div></div>
                <div className="authorityLevelAdjustment">
                    <input
                        autoComplete='off'
                        type='number'
                        placeholder='Update Level'
                        value={adjustAuthorityLevel}
                        min='1'
                        max='5'
                        onChange={
                            (event) => {
                                setAdjustAuthorityLevel(parseInt(event.currentTarget.value))
                            }
                        }
                    >
                    </input>

                    <button
                        onClick={
                            async () => {
                                await post('/adjustAuthorityLevel', {
                                    updateLv: adjustAuthorityLevel,
                                    staffId:props.selectedStaffId
                                })
                                setAdjustAuthorityLevel(0);

                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success!!',
                                    text: 'You have updated the Authority Level!'
                                  })

                            }
                        }
                    >
                        <p>Summit</p>
                    </button>

                </div>


                <div className="currentSalaryAdjustment">

                <input
                        autoComplete='off'
                        type='number'
                        placeholder='Update Salary'
                        value={adjustSalary}
                        onChange={
                            (event) => {
                                setAdjustSalary(parseInt(event.currentTarget.value))
                            }
                        }
                    >
                    </input>

                    <button
                        onClick={
                            async () => {
                                await post('/adjustSalary', {
                                   updateSalary: adjustSalary,
                                    staffId:props.selectedStaffId
                                })
                                setAdjustSalary(0);
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success!!',
                                    text: 'You have updated the Salary!'
                                  })
                            }
                        }
                    >
                        <p>Summit</p>
                    </button>


                </div>

            </div>
        </div>
    )
} 