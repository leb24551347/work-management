import { StaffDetail } from 'common';
import React, { useEffect } from 'react';
import { useState } from 'react';
import { post } from '../../../../api';
import { convertToYMD, formatDateTime } from '../../../../format';
// import { SearchBar } from "../SearchingBar/SearchingBar";
import './PersonalAttendanceRecord.scss';

export function PersonalAttendanceRecord(
    props: {
        selectedStaffId: number
    }) {

    const [lateRecord, setLateRecord] = useState<StaffDetail[]>([]);
    const [leaveRecord, setLeaveRecord] = useState<StaffDetail[]>([]);
    async function getStaffLateRecord(staffId: number) {
        //console.log("🚀 ~ file: LateRecord.tsx ~ line 15 ~ staffId",staffId)
        const res = await post('/getStaffLateRecord', { staffId: staffId });
        const json = await res.json();
        setLateRecord(json);
    }

    async function getStaffLeaveRecord(staffId: number) {
        const res = await post('/getStaffLeaveRecord', { staffId: staffId });
        const json = await res.json();         
        setLeaveRecord(json);
    }
    

    useEffect(
        () => {
      
            getStaffLateRecord(props.selectedStaffId);
            getStaffLeaveRecord(props.selectedStaffId);
        }, [props.selectedStaffId]

    )
    return (

        <div className="PersonalAttendanceRecord">

            <div className="PersonalAttendanceRecordMenu">
                {/* <div className="MenuLateDate">LateDate</div> */}
                {/* <div className="MenuLeave Day">Leave Date</div> */}
                {/* <div className="barAlateTime">Arrive Time</div> */}
            </div>

            <div className="PersonalAttendanceRecordList">
                <div className="PersonalAttendanceRecordLateDate">
                    <div className="MenuLateDate">LateDate</div>
                    {
                        Object.keys(lateRecord).map(itemKey => {
                            const returnTime = lateRecord[parseInt(itemKey)].checkin_time
                            const formatedTime = formatDateTime(returnTime);
                            return (
                                <tr key={itemKey}>
                                    <td>{formatedTime}</td>
                                </tr>
                            )

                        })
                    }
                </div>
                <div className="PersonalAttendanceRecordLeaveDate">
                    <div className="MenuLeave Day">Leave Date</div>
                    {
                        Object.keys(leaveRecord).map(itemKey => {
                            
                            const start_time = convertToYMD((leaveRecord[0].start_time).toString());
                            console.log("🚀 ~ file: PersonalAttendanceRecord.tsx ~ line 72 ~ Object.keys ~ result", start_time)
                            const end_time = convertToYMD((leaveRecord[0].end_time).toString());

                            return (
                                <tr key={itemKey}>
                                    <td>From{start_time} To {end_time}</td>
                                    <td>({leaveRecord[parseInt(itemKey)].vacation_name})</td>
                                </tr>
                            )

                        })
                    }
                </div>
            </div>

        </div>

    )
}