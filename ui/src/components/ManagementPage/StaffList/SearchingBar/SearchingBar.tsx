import React from 'react';
import { propTypes } from 'react-bootstrap/esm/Image';
import "./SearchingBar.scss"

export const SearchBar = () => {
    //const BarStyling = {width:"20rem",height:"2rem",background:"#F2F1F9", border:"none", padding:"0.5rem"};
    return (
        <div className="searchingBar">
            <label htmlFor="search">
                <span className="visually-hidden">
                    Search by name
                </span>
            </label>

            <input
                type="text"  
                placeholder="Search..."
                
            />

            <button type="submit">Search</button>
       </div>
    );
}