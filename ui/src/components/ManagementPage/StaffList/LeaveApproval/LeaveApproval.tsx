import { StaffDetail } from 'common';
import React, { useEffect, useState } from 'react';
import { get, post } from '../../../../api';
import { convertToYMD } from '../../../../format';
import "./LeaveApproval.scss";
import Swal from 'sweetalert2';

export function LeaveApproval() {
    const [leaveApplyList, setLeaveApplyList] = useState<StaffDetail[]>([]);

    useEffect(
        () => {
            async function leaveApply() {
                const res = await get('/getLeaveApply');
                const json = await res.json();
                setLeaveApplyList(json);
            }                        
            leaveApply()
        }, [leaveApplyList]
    )

    return (
        <div className="leaveApproval">
            <div className="leaveApprovalBar">
                <div className="leaveApprovalStaffName">Staff Name</div>
                <div className="leaveApprovalDepartment">Department</div>
                <div className="leaveApprovalStartTime">Start Date</div>
                <div className="leaveApprovalEndTime">End Date</div>
                <div className="leaveApprovalLeaveType">LeaveType</div>
                <div className="leaveApprovalStatus">Status</div>

            </div>

            <div className="leaveApprovalList">

                {
                    Object.keys(leaveApplyList).map(itemKey => {
                        const staff_leave_id =leaveApplyList[parseInt(itemKey)].id
                        const startDate = convertToYMD(leaveApplyList[parseInt(itemKey)].start_time.toString())
                        const endDate = convertToYMD(leaveApplyList[parseInt(itemKey)].end_time.toString())
                        return (
                            <tr key={itemKey}>
                                {/* <td> {SIA[parseInt(itemKey)].authority_level}</td> */}
                                <td>{leaveApplyList[parseInt(itemKey)].username}</td>
                                <td>{leaveApplyList[parseInt(itemKey)].department_name}</td>
                                <td>{startDate}</td>
                                <td>{endDate}</td>
                                <td>{leaveApplyList[parseInt(itemKey)].vacation_name}</td>
                                <td>
                                <button className="acceptButton"
                                onClick={async ()=>{
                                    
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Success!!',
                                        text: 'Application Accepted!'
                                      });
                                      await post('/setLeaveApplyResult',{result:"accept",leaveApplyId:staff_leave_id});
                                      
                                }}
                                >Accept</button>

                                <button className="rejectButton"
                                onClick={async ()=>{
                                    
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Success!!',
                                        text: 'Application Rejected!'
                                      });
                                      await post('/setLeaveApplyResult',{result:"reject",leaveApplyId:staff_leave_id});
                                      
                                }}
                                >Reject</button>
                                </td>
                            </tr>
                        )
                    })
                }
            </div>
        </div>
    )
} 