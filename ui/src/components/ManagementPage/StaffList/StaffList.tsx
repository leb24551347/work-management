import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, Route, Switch } from 'react-router-dom';
import { RootState } from '../../../store';

import { PersonalAttendanceRecord } from './LateRecord/PersonalAttendanceRecord';
import { LeaveApproval } from './LeaveApproval/LeaveApproval';
import { StaffInfoAdjustment } from './SatffInfoAdjustment/StaffInfoAdjustment';
import { SelectSearch } from './SelectSearch/SelectSearch';
// import { SearchBar } from './SearchingBar/SearchingBar';
import './StaffList.scss';

export function StaffList() {
    let selectrfStaffId = useSelector((state: RootState) => state.StaffList.staffId)
    const [currentButton, setCurrentButton] = useState('');
    
    return (
        <div>
            <div className="searchingBar">
            <SelectSearch/>
            </div>

            <div className="staffInfoBlock">
                <div>
                    <div className="title">

                        <Link to="/ManagementPage/stafflist/personalattendancerecord">
                            <button 
                             style={{backgroundColor: currentButton === 'personalattendancerecord' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)'}}
                             onClick={() => setCurrentButton('personalattendancerecord')}
                            >Attendance record</button>
                            </Link>

                        <Link to="/ManagementPage/stafflist/staffinformationadjustment">
                            <button 
                            className="staffinformationadjustment"
                            style={{backgroundColor: currentButton === 'staffinformationadjustment' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)'}}
                            onClick={() => setCurrentButton('staffinformationadjustment')}
                            >S.I.A</button>
                            </Link>

                        <Link to="/ManagementPage/stafflist/salaryrecord">
                            <button 
                            style={{backgroundColor: currentButton === 'salaryrecord' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)'}}
                            onClick={() => setCurrentButton('salaryrecord')}
                            >Leave Approval</button>
                            </Link>
                    </div>
                </div>

                <div>
                    <div className="info">
                        <Switch>
                            <Route path='/ManagementPage/stafflist/personalattendancerecord'>
                                <PersonalAttendanceRecord selectedStaffId={selectrfStaffId}/>
                            </Route>

                            <Route path='/ManagementPage/stafflist/staffinformationadjustment'>
                            <StaffInfoAdjustment selectedStaffId={selectrfStaffId}/>
                            </Route>

                            <Route path='/ManagementPage/stafflist/salaryrecord'>
                            <LeaveApproval />
                            </Route>


                        </Switch>
                    </div>
                    {/* <div>ID:{selectrfStaffId} </div> */}
                </div>

            </div>

        </div>
    );

}
