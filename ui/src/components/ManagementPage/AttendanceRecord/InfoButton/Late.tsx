import React, { useEffect, useState } from 'react';
import type { StaffDetail } from 'common';
import './Late.scss';
import {post} from '../../../../api';
import { convertToHHMM } from '../../../../format';

export function Late(props:{
    selectedDate:string;
}) {

    const [lateRecord, setLateRecord] = useState<StaffDetail[]>([]);

   

    useEffect(
        () => {
            async function ManagementGetLaterecord(date:string) {

                const res = await post('/lateRecord',{'selectedDate':date});
        
                const json: StaffDetail[] = await res.json();
                setLateRecord(json);
            };

            ManagementGetLaterecord(props.selectedDate);
        }
            
        , [props.selectedDate]
    )
    
    


    return (
        <div>
            <div className="detailBar">
                <div className="barStaffName">StaffName</div>
                <div className="barDepartment">Department</div>
                <div className="barLateTime">LateTime</div>
            </div>

            <div className="detailList">

                <div className="StaffName">
                    {
                        Object.keys(lateRecord).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {lateRecord[parseInt(itemKey)].username}</td>
                                </tr>
                            )

                        })
                    }
                </div>

                <div className="department">
                    {
                        Object.keys(lateRecord).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {lateRecord[parseInt(itemKey)].department_name}</td>
                                </tr>
                            )

                        })
                    }

                </div>

                <div className="arriveTime">
                {
                    
                        Object.keys(lateRecord).map(itemKey => {
                            const returnTime = convertToHHMM(lateRecord[parseInt(itemKey)].checkin_time)
                            return (
                                <tr key={itemKey}>
                                    {/* <td> {getHour}:{getMinute}</td> */}
                                    <td>{returnTime}</td>
                                </tr>
                            )

                        })
                    }

            
                </div>

            </div>
        </div>
    )
}