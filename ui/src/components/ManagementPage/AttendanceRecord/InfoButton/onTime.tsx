import React, { useEffect, useState } from 'react';
import type { StaffDetail } from 'common';
import './onTime.scss';
import { post } from '../../../../api';
import { convertToHHMM } from '../../../../format';

export function OnTime(props: { selectedDate: string }) {
    const [onTimeRecord, setOnTimeRecord] = useState<StaffDetail[]>([]);


    useEffect(
        () => {
            async function getOnTimeRecord(date:string) {
                const res = await post('/onTimeRecord',{'selectedDate':date});        
                const json: StaffDetail[] = await res.json();      
                setOnTimeRecord(json);       
            };
            
            getOnTimeRecord(props.selectedDate);
        }
            
        , [props.selectedDate]
    )
    
    
    return (
        <div>
            <div className='detailBar'>
                <div className='barStaffName'>StaffName</div>
                <div className='barDepartment'>Department</div>
                <div className='barArriveTime'>Arrive Time</div>
            </div>

            <div className='detailList'>
                <div className='StaffName'>
                    {Object.keys(onTimeRecord).map((itemKey) => {
                        return (
                            <tr key={itemKey}>
                                <td> {onTimeRecord[parseInt(itemKey)].username}</td>
                            </tr>
                        );
                    })}
                </div>

                <div className='department'>
                    {Object.keys(onTimeRecord).map((itemKey) => {
                        return (
                            <tr key={itemKey}>
                                <td> {onTimeRecord[parseInt(itemKey)].department_name}</td>
                            </tr>
                        );
                    })}
                </div>

                <div className='arriveTime'>
                    {Object.keys(onTimeRecord).map((itemKey) => {
                        const returnTime = convertToHHMM(onTimeRecord[parseInt(itemKey)].checkin_time);
                        return (
                            <tr key={itemKey}>
                                <td>{returnTime}</td>
                            </tr>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}
