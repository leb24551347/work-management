import React, { useEffect, useState } from 'react';
import type { StaffDetail } from 'common';
import './Leave.scss';
import { post } from '../../../../api';

export function Leave(props: {
    selectedDate: string;
}
) {
    const [leaveRecord, setLeaveRecord] = useState<StaffDetail[]>([]);

    
    useEffect(
        () => {

            async function LeaveRecord(date: string) {

                const res = await post('/leaveRecord', { 'selectedDate': date });
        
                const json: StaffDetail[] = await res.json();      
               setLeaveRecord(json);
            };
        
            
            LeaveRecord(props.selectedDate);
        }

        , [props.selectedDate]
    )

    return (
        <div>
            <div className="detailBar">
                <div className="barStaffName">StaffName</div>
                <div className="barDepartment">Department</div>
                <div className="barLeaveType">Leave Type</div>
            </div>

            <div className="detailList">

                <div className="leaveStaffName">
                    {
                        Object.keys(leaveRecord).map(itemKey => {

                                return (
                                    <tr key={itemKey}>
                                        <td> {leaveRecord[parseInt(itemKey)].username}</td>
                                    </tr>
                                )
                            

                        })
                    }
                </div>

                <div className="leaveDepartment">
                    {
                        Object.keys(leaveRecord).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {leaveRecord[parseInt(itemKey)].department_name}</td>
                                </tr>
                            )
                        })
                    }

                </div>

                <div className="leaveDetail">
                    {

                        Object.keys(leaveRecord).map(itemKey => {
                            return (
                                <tr key={itemKey}>
                                    <td> {leaveRecord[parseInt(itemKey)].vacation_name}</td>
                                </tr>
                            )

                        })
                    }
                </div>

            </div>
        </div>
    )
}