import React, { useState } from 'react';
import './AttendanceRecord.scss';
import { OnTime } from './InfoButton/onTime';
import { Late } from './InfoButton/Late';
import { Leave } from './InfoButton/Leave';
import { FaCalendarAlt } from "react-icons/fa";
import { useDispatch, useSelector } from 'react-redux';
import { SelectDate } from '../../../redux/ManagementPage/AttendanceRecord/action';
import { RootState } from '../../../store';
import { Link, Route, Switch } from 'react-router-dom';



export function AttendanceRecord() {
    let selectDate = useSelector((state: RootState) => state.AttendanceRecord.selectDate)

   let ISOString = new Date(selectDate).toISOString();
   console.log("🚀 ~ file: AttendanceRecord.tsx ~ line 18 ~ AttendanceRecord ~ ISOString", ISOString)
   

    const dispatch = useDispatch();
 const [clickedButton,setClickedButton] = useState('')

    return (
        <div className="AttendanceRecord">

            <div className="recordButtonContainer">
                <div className="recordButton">

                    <Link to="/ManagementPage/attendancerecord/ontime">
                        <button className="onTime"
                        style={{backgroundColor: clickedButton === 'onTime' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)'}}
                        onClick={() => setClickedButton('onTime')}
                        >ontime</button>
                        </Link>

                    <Link to="/ManagementPage/attendancerecord/late">
                        <button className="late" 
                         style={{backgroundColor: clickedButton === 'late' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)'}}
                         onClick={() => setClickedButton('late')}
                        >Late</button>
                    </Link>

                    <Link to="/ManagementPage/attendancerecord/leave">
                        <button className="Leave" 
                        style={{backgroundColor: clickedButton === 'Leave' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)'}}
                        onClick={() => setClickedButton('Leave')}
                        >Leave</button>
                    </Link>
                </div>
            </div>
            <div className="dateSelect">

                <div className="date">
                    <div>
                        <FaCalendarAlt className="calendar" type="button" />
                        <input type="date" onChange={
                            (event) => {
                                dispatch(SelectDate(event.target.value))
                            }
                        } />
                    </div>

                    <div>
                        {selectDate}
                    </div>

                </div>

            </div>

            <div>
                {/* empty div */}
            </div>

            <div className="recordContent">

                <Switch>

                    <Route path='/ManagementPage/attendancerecord/ontime'>
                        <OnTime selectedDate={selectDate}/>
                    </Route>

                    <Route path='/ManagementPage/attendancerecord/late'>
                        <Late selectedDate={selectDate}/>
                    </Route>

                    <Route path='/ManagementPage/attendancerecord/leave'>
                        <Leave selectedDate={selectDate}/>
                    </Route>

                </Switch>
            </div>


        </div>
    );

}