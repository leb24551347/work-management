import React, { useState } from 'react';
import { useEffect } from 'react';
import { get } from '../../api';
import './getHeartBeat.scss';
import { formatDateTime, formatDuration } from '../../format';

export function HeartBeatDataList() {
    type heartBeatDataType = {
        username: string;
        heartBeat_time: string;
        checkin_time: string;
    };

    const [heartBeatData, setHeartBeatData] = useState<heartBeatDataType[]>([]);

    useEffect(() => {
        (async () => {
            const res = await get('/getHeartBeatData');
            const json = await res.json();
            console.log('get heart beat json data: ', json);
            setHeartBeatData(json.heartBeatData);
        })();
    }, []);

    return (
        <div className='getHeartBeat-header'>
            <div className='getHeartBeat-Area'>
                <div className='getHeartBeat-name'>Staff Heart Beat Time List</div>

                <div className='getHeartBeatList-content desktop'>
                    <table>
                        <tr>
                            <th className='getHeartBeat-username'>Staff Name</th>
                            <th className='getHeartBeat-lastClockIn'>Last Clock In</th>
                            <th className='getHeartBeat-lastHeartBeat'>Last Heart Beat</th>
                            <th className='getHeartBeat-since'>Since Last HB</th>
                        </tr>

                        {heartBeatData.map((heartBeat) => (
                            <tr>
                                <td className='heartBeatType-item'>{heartBeat.username}</td>
                                <td className='heartBeatType-item'>{formatDateTime(heartBeat.checkin_time)}</td>
                                <td className='heartBeatType-item'>{formatDateTime(new Date(heartBeat.heartBeat_time) + '')}</td>
                                <td className='heartBeatType-item'>{formatDuration(Date.now() - new Date(heartBeat.heartBeat_time).getTime())}</td>
                            </tr>
                        ))}
                    </table>
                </div>

                <div className='getHeartBeatList-content mobile'>
                    <table>
                        <tr>
                            <th className='getHeartBeat-username'>Staff Name</th>

                            <th className='getHeartBeat-lastHeartBeat'>Last Heart Beat</th>
                            <th className='getHeartBeat-since'>Since Last HB</th>
                        </tr>

                        {heartBeatData.map((heartBeat) => (
                            <tr>
                                <td className='heartBeatType-item'>{heartBeat.username}</td>

                                <td className='heartBeatType-item'>{formatDateTime(new Date(heartBeat.heartBeat_time) + '')}</td>
                                <td className='heartBeatType-item'>{formatDuration(Date.now() - new Date(heartBeat.heartBeat_time).getTime())}</td>
                            </tr>
                        ))}
                    </table>
                </div>
            </div>
        </div>
    );
}

export default HeartBeatDataList;
