import React, { useState } from 'react';
import { useEffect } from 'react';
import { get } from '../../api';
import './AttendanceList.scss';
import { formatDateTime, formatDuration } from '../../format';
import Swal from 'sweetalert2';

export function AttendanceList() {
    const [fullDate] = formatDateTime(new Date().toISOString()).split(' ');
    const [startDate, setStartDate] = useState(fullDate);
    const [endDate, setEndDate] = useState(fullDate);

    const start = new Date(startDate + ' 00:00');
    const end = new Date(endDate + ' 00:00');

    end.setTime(end.getTime() + 1000 * 60 * 60 * 24);

    const startStr = start.toISOString();
    const endStr = end.toISOString();

    async function getAttendanceData() {
        console.log('this is get Staff Attendance range data route');

        if (startDate > endDate) {
            console.log('End Date Must More Than The Start Date');
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'End Date Must More Than The Start Date',
            });
            return;
        }

        const res = await get(`/getAttendanceRangeData?start=${startStr}&end=${endStr}`);
        const json = await res.json();
        setAttendanceData(json.getAttendanceRangeData);
    }

    type attendanceDataType = {
        id: number;
        checkin_time: string;
        checkout_time: string;
    };

    const [AttendanceData, setAttendanceData] = useState<attendanceDataType[]>([]);

    useEffect(() => {
        (async () => {
            const res = await get('/getStaffAttendance');
            const json = await res.json();

            setAttendanceData(json.getStaffAttendance);
        })();
    }, []);

    return (
        <div className='attendanceList-header'>
            <div className='attendanceList-Area'>
                <div className='attendanceList-name'>Attendance List</div>
                <div className='submitAttendanceDateArea'>
                    <div className='attendanceStartArea'>
                        <div className='attendanceStartAreaName'>Start Time</div>
                        <input type='date' id='startDate' value={startDate} onChange={(event) => setStartDate(event.currentTarget.value)} />
                    </div>
                    <div className='attendanceEndArea'>
                        <div className='attendanceEndAreaName'>End Time</div>
                        <input type='date' id='startDate' value={endDate} onChange={(event) => setEndDate(event.currentTarget.value)} />
                    </div>
                    <button className='apply-button' type='submit' onClick={getAttendanceData}>
                        Apply
                    </button>
                </div>

                <div className='staffAttendanceStatusList-content'>
                    <table>
                        <tr>
                            <th className='staffAttendanceStatusList-created_at'>Clock In Time</th>
                            <th className='staffAttendanceStatusList-start_time'>Clock Out Time</th>
                            <th className='staffAttendanceStatusList-work_hour'>Work Hour</th>
                        </tr>

                        {AttendanceData.map((attendance) => (
                            <tr>
                                <td className='attendanceType-item'>{formatDateTime(attendance.checkin_time)}</td>
                                <td className='attendanceType-item'>{formatDateTime(attendance.checkout_time)}</td>
                                <td className='attendanceType-item'>
                                    {formatDuration(new Date(attendance.checkout_time).getTime() - new Date(attendance.checkin_time).getTime())}
                                </td>
                            </tr>
                        ))}
                    </table>
                </div>
            </div>
        </div>
    );
}

export default AttendanceList;
