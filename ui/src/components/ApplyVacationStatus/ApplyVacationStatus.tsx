import React, { useState } from 'react';
// import { useEffect } from 'react';
import { get } from '../../api';
import './ApplyVacationStatus.scss';
import { formatDateTime } from '../../format';
import Swal from 'sweetalert2';

export function ApplyVacationStatus() {
    const [fullDate] = formatDateTime(new Date().toISOString()).split(' ');
    const [startDate, setStartDate] = useState(fullDate);
    const [endDate, setEndDate] = useState(fullDate);

    const start = new Date(startDate + ' 00:00');
    const end = new Date(endDate + ' 00:00');

    end.setTime(end.getTime() + 1000 * 60 * 60 * 24);

    const startStr = start.toISOString();
    const endStr = end.toISOString();

    async function getAllVacationStatus() {
        console.log('get vacation status');

        if (startDate > endDate) {
            console.log('End Date Must More Than The Start Date');
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'End Date Must More Than The Start Date',
            });
            return;
        }
        const res = await get(`/getVacationData?start=${startStr}&end=${endStr}`);
        const json = await res.json();
        setGetVacationStatus(json.secifyApplyVacation);
    }

    type vacationDataType = {
        id: number;
        vacation_name: string;
        status: string;
        start_time: string;
        end_time: string;
        created_at: string;
        updated_at: string;
    };
    const [getVacationStatus, setGetVacationStatus] = useState<vacationDataType[]>([]);

    // useEffect(() => {
    //     (async () => {
    //         console.log('load vacation type fetch');
    //         const res = await get('/getVacationStatus');
    //         const json = await res.json();
    //         setGetVacationStatus(json.vacationData);
    //     })();
    // }, []);

    return (
        <div className='staffVacationList-header'>
            <div className='staffVacationList-Area'>
                <div className='staffVacationList-name'>Apply Vacation Stauts</div>
                <div className='submitVacationDateArea'>
                    <div className='vacationStartArea'>
                        <div className='vacationStartAreaName'>Start Time</div>
                        <input type='date' id='startDate' value={startDate} onChange={(event) => setStartDate(event.currentTarget.value)} />
                    </div>

                    <div className='vacationEndArea'>
                        <div className='vacationEndAreaName'>End Time</div>
                        <input type='date' id='startDate' value={endDate} onChange={(event) => setEndDate(event.currentTarget.value)} />
                    </div>
                    <button className='apply-button' type='submit' onClick={getAllVacationStatus}>
                        Apply
                    </button>
                </div>
                <div className='staffVacationList-content desktop'>
                    <table>
                        <tr>
                            <th className='staffVacationList-created_at'>Application Date</th>
                            <th className='staffVacationList-type'>Vacation Type</th>
                            <th className='staffVacationList-start_time'>Start Time</th>
                            <th className='staffVacationList-end_time'>Finish Time</th>
                            <th className='staffVacationList-status'>Status</th>
                            <th className='staffVacationList-update'>Latest Update</th>
                        </tr>

                        {getVacationStatus.map((vacation) => (
                            <tr>
                                <td className='vacationType-item'>{formatDateTime(vacation.created_at)}</td>
                                <td className='vacationType-item'>{vacation.vacation_name}</td>
                                <td className='vacationType-item'>{formatDateTime(vacation.start_time)}</td>
                                <td className='vacationType-item'>{formatDateTime(vacation.end_time)}</td>
                                <td className='vacationType-item'> {vacation.status}</td>
                                <td className='vacationType-item'> {formatDateTime(vacation.updated_at)}</td>
                            </tr>
                        ))}
                    </table>
                </div>

                <div className='staffVacationList-content mobile'>
                    <table>
                        <tr>
                            <th className='staffVacationList-created_at'>Application Date</th>
                            <th className='staffVacationList-type'>Vacation Type</th>
                            <th className='staffVacationList-status'>Status</th>
                        </tr>

                        {getVacationStatus.map((vacation) => (
                            <tr>
                                <td className='vacationType-item'>{formatDateTime(vacation.created_at)}</td>
                                <td className='vacationType-item'>{vacation.vacation_name}</td>
                                <td className='vacationType-item'> {vacation.status}</td>
                            </tr>
                        ))}
                    </table>
                </div>
            </div>
        </div>
    );
}

export default ApplyVacationStatus;
