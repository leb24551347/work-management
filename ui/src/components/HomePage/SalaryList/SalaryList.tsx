import { useState } from 'react';
import React, { useEffect } from 'react';
import './SalaryList.scss';
import { GiMoneyStack } from 'react-icons/gi';
import { get } from '../../../api';
import { RootState } from '../../../store';
import { useSelector } from 'react-redux';

//datatype

type EmployeeData = {
    username: string;
    id: number;
    department_name: string;
    salary: number;
    bonus: number;
    allowance: number;
    ot_hours: number;
};

interface Props {
    month: string;
}

// interface ChildSalaryProps {
//     monthSalary: number;
// }

// export function SalaryList(props: ChildSalaryProps) {
export function SalaryList(props: Props) {

    const [employeeData, setEmployeeData] = useState<EmployeeData | null>(null);


    useEffect(() => {
        async function getEmployeeData() {
            // let date = new Date(selectMonth + ' 00:00');
            const res = await get(`/EmployeeInfo?month=${props.month}`);
            const json = await res.json();
            setEmployeeData(json);
        }
        getEmployeeData();
    }, [props.month]);

    if (employeeData == null) {
        return (
            <div className='content-background'>
                <div className='employee-content'>Staff detail have not filled</div>
            </div>
        );
    }
    //  console.log(employeeData);

    return (
        <div className='content-background'>
            <div className='employee-content'>
                <div className='head-col'>
                    <div>Employee:</div>
                    <div className='head-content'>{employeeData.username}</div>
                </div>
                <div className='head-col'>
                    <div>Staff id:</div>
                    <div className='head-content'>{employeeData.id}</div>
                </div>
                <div className='head-col'>
                    <div>Department:</div>
                    <div className='head-content'>{employeeData.department_name}</div>
                </div>
            </div>

            <div className='summary-header'>
                <div className='Fa-icon'>
                    <h1>
                        <GiMoneyStack /> Salary Summary (HKD){' '}
                    </h1>
                </div>
            </div>
            <div className='basic-item'>
                <div className='sum'>
                    <table className='tableList'>
                        <tbody>
                            <tr className='salaryList'>
                                {/* <td>Salary payable: {props.monthSalary * 10000}</td> */}
                                <td className='SalaryList-type'>Salary payable:</td>
                                <td className='SalaryList-type'>{employeeData.salary}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className='sum'>
                    <table className='tableList'>
                        <tbody>
                            <tr className='salaryList'>
                                <td className='SalaryList-type'>Allowance:</td>
                                <td className='SalaryList-type'>{employeeData.allowance}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className='sum'>
                    <table className='tableList'>
                        <tbody>
                            <tr className='salaryList'>
                                <td className='SalaryList-type'>Bonus:</td>
                                <td className='SalaryList-type'>{employeeData.bonus}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div className='sum'>
                    <table className='tableList'>
                        <tbody>
                            <tr className='salaryList'>
                                <td className='SalaryList-type'>Actual salary paid:</td>
                                <td className='SalaryList-type'>{employeeData.salary + employeeData.allowance + employeeData.bonus}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {/* <div>Month{selectMonth}</div>    */}
            </div>
        </div>
    );
}

export default SalaryList;
