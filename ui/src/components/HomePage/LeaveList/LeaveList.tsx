import { useState } from 'react';
import React, { useEffect } from 'react';
import './LeaveList.scss';
import { FaBed } from 'react-icons/fa';
import { get } from '../../../api';
import { RootState } from '../../../store';
import { useSelector } from 'react-redux';

type EmployeeData = {
    duration: Duration;
    leave_duration: Duration;
};

type Duration = {
    days?: number;
    hours?: number;
    minutes?: number;
};

interface Props {
    month: string;
}

// interface ChildSalaryProps {
//     monthSalary: number;
// }

export function LeaveList(props: Props) {

    const [employeeData, setEmployeeData] = useState<EmployeeData | null>(null);
    // const [duration, setDuration] = useState<Duration[]>([]);

    
    useEffect(() => {
        async function getEmployeeData() {
            // let date = new Date(selectMonth + ' 00:00');
            const res = await get(`/EmployeeInfo?month=${props.month}`);
            const json = await res.json();
            setEmployeeData(json);
            // setDuration
        }
        getEmployeeData();
    }, [props.month]);

    if (employeeData == null) {
        return (
            <div className='content-background'>
                <div className='employee-content'>Staff detail have not filled</div>
            </div>
        );
    }
    //  console.log(employeeData);

    return (
        <div className='content-background'>
            <div className='alLeave-header'>
                <div className='Fa-icon'>
                    <h1>
                        <FaBed /> Leaves (days)
                    </h1>
                </div>
            </div>
            <div className='basic-item'>
                {/* <div className='sum'>
                    <table>
                        <tr>
                            <td>Sick Leave:</td>
                            <td>{employeeData.sick_leave}</td>
                        </tr>
                    </table>
                </div>
                <div className='sum'>
                    <table>
                        <tr>
                            <td>Annual Leave:</td>
                            <td>{employeeData.annual_leave}</td>
                        </tr>
                    </table>
                </div>
                <div className='sum'>
                    <table>
                        <tr>
                            <td>Casual Leave:</td>
                            <td>{employeeData.casual_leave}</td>
                        </tr>
                    </table>
                </div>
                <div className='sum'>
                    <table>
                        <tr>
                            <td>Marriage Leave:</td>
                            <td>{employeeData.marriage_leave}</td>
                        </tr>
                    </table>
                </div>
                <div className='sum'>
                    <table>
                        <tr>
                            <td>Maternity Leave:</td>
                            <td>{employeeData.maternity_leave}</td>
                        </tr>
                    </table>
                </div>
                <div className='sum'>
                    <table>
                        <tr>
                            <td>Paternity Leave:</td>
                            <td>{employeeData.paternity_leave}</td>
                        </tr>
                    </table>
                </div>
                <div className='sum'>
                    <table>
                        <tr>
                            <td>Jury Service Leave:</td>
                            <td>{employeeData.jury_service_leave}</td>
                        </tr>
                    </table>
                </div> */}
                <div className='sum'>
                    <table className='leaveTableList'>
                        <tbody>
                        <tr className='leaveList'>
                            <td className='leaveList-type'>Total leaves taken:</td>
                            <td className='leaveList-type'>{employeeData.leave_duration?.days}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div className='sum'>
                    <table className='leaveTableList'>
                    <tbody>
                        <tr className='leaveList'>
                            <td className='leaveList-type'>Effective working days:</td>
                            <td className='leaveList-type'>{employeeData.duration?.hours}</td>
                            {/* <td className='leaveList-type'>20</td> */}
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default LeaveList;
