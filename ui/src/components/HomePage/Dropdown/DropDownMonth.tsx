
import { get } from '../../../api';
import React, { useEffect, useState } from 'react';
import './DropDownMonth.scss';

type monthInfoType = {
  id: number;
  start_time: string;
  end_time: string;
}

interface Props {
  onChangeMonth: (month: string) => void;
  month: string;
}
export function DropDownMonth(props: Props) {
  const getCurrentYear = () => {
    return new Date().getFullYear();
  };

  return (

    <div className='periodContainer'>

      <div className="displayYear">Year: {getCurrentYear()}</div>
      <select className='dropdown' value={props.month}
        onChange={(e) => props.onChangeMonth(e.target.value)}>
        <option value="">Please select a month...</option>
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
    </div>


  );
}

export default DropDownMonth;