import React, { useState } from 'react';
import './DropDownMonth.scss';





function DropDownYear (props: { 
  
  Year?: string;
  // onClick: () => void;
 
}) {
  // let selectYear = useSelector((state: RootState) => state.yearSummary.selectYear)


  const [selectedYear, setSelectedYear] = useState("");

  
  

  return (
  <div className='periodContainer'>
     <input className='dropdown'
     type="number"
     placeholder = "Enter the year..."
     value={selectedYear}
     onChange={e=>setSelectedYear(e.target.value)}
     >
       
     </input> 
  </div>
  
  );
}

export default DropDownYear;