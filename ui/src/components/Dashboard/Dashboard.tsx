import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Dashboard.scss';
import { FaMix } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { RiLockPasswordLine } from 'react-icons/ri';
import { BsClock } from 'react-icons/bs';
import { BsFillPeopleFill } from 'react-icons/bs';
import { FaHome } from 'react-icons/fa';
import { FaAddressCard } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import { logoutSuccess } from '../../redux/auth/action';
import { RootState } from '../../store';
import { useEffect } from 'react';
// import { ImExit } from 'react-icons/im';

function Dashboard() {
    const staff = useSelector((state: RootState) => state.auth.user);
    const [currentDashButton, setCurrentDashButton] = useState('');

    // const staffName = useSelector((state: RootState) => state.auth.username);
    // const department_id = useSelector((state: RootState) => state.auth.department_id);
    // const authority_level = useSelector((state: RootState) => state.auth.authority_level);

    // console.log('authority_level: ' + authority_level + ' && ', 'department_id: ' + department_id);
    // console.log('name', staff);
    const dispatch = useDispatch();

    useEffect(() => {
        console.log('dashboard header loaded');
    }, []);

    // useEffect(() => {
    //     console.log(`staffName is ${staffName}`);
    // }, [staffName]);

    return (
        <div className='dashboard'>
            <header className='dashboard-area'>
                {staff && (
                    <>
                        <div className='dashboard-header'>
                            <div className='logoArea'>
                                <div className='imgLogo desktop'>
                                    Work <FaMix />
                                    anagement
                                </div>
                                <div className='imgLogo mobile'>
                                    <FaMix />
                                </div>
                            </div>
                            <div className='changeButton'>
                                <div className='icon'>
                                    <Link to='/homepage'>
                                        <FaHome 
                                        className='headerButton' 
                                        type='button' 
                                        style={{color:currentDashButton==='homepage'?'#f2b463' : 'grey'}}
                                        onClick={() => setCurrentDashButton('homepage')}
                                        />
                                    </Link>
                                    <span className='tooltiptext'>Home</span>
                                </div>
                                <div className='icon'>
                                    <Link to='/clockIn'>
                                        <BsClock 
                                        className='headerButton' 
                                        type='button' 
                                        style={{color:currentDashButton==='clockIn'?'#f2b463' : 'grey'}}
                                        onClick={() => setCurrentDashButton('clockIn')}
                                        />
                                    </Link>
                                    <span className='tooltiptext'>Clock In</span>
                                </div>
                                <div className='icon'>
                                    <Link to='/changePassword'>
                                        <RiLockPasswordLine 
                                        className='headerButton' 
                                        type='button' 
                                        style={{color:currentDashButton==='changePassword'?'#f2b463' : 'grey'}}
                                        onClick={() => setCurrentDashButton('changePassword')}
                                        />
                                    </Link>
                                    <span className='tooltiptext'>Change Password</span>
                                </div>

                                {/* 上司function */}
                                {staff.authority_level >= 4 && (
                                    <>
                                        <div className='icon'>
                                            <Link to='/register'>
                                                <BsFillPeopleFill 
                                                className='headerButton' 
                                                type='button' 
                                                style={{color:currentDashButton==='register'?'#f2b463' : 'grey'}}
                                                onClick={() => setCurrentDashButton('register')}
                                                />
                                            </Link>
                                            <span className='tooltiptext'>Create Staff</span>
                                        </div>

                                        <div className='icon'>
                                            <Link to='/ManagementPage'>
                                                <FaAddressCard 
                                                className='headerButton' 
                                                type='button' 
                                                style={{color:currentDashButton==='ManagementPage'?'#f2b463' : 'grey'}}
                                                onClick={() => setCurrentDashButton('ManagementPage')}
                                                />
                                            </Link>
                                            <span className='tooltiptext'>Staff Management</span>
                                        </div>
                                    </>
                                )}
                            </div>
                            <div className='staffArea'>
                                <div className='staffName'>{staff.username}</div>
                                <button
                                    className='logoutButton'
                                    onClick={() => {
                                        dispatch(logoutSuccess());
                                        localStorage.removeItem('token');
                                        dispatch(push('/'));
                                    }}
                                >
                                    logout
                                </button>
                            </div>
                        </div>
                    </>
                )}
            </header>
        </div>
    );
}

export default Dashboard;
