import React, { useState } from 'react';
import { post } from '../../api';
import './otList.scss';
import Swal from 'sweetalert2';

export function OtList() {
    const initialDay = new Date();

    let month = '0' + (initialDay.getMonth() + 1);
    month = month.substring(month.length - 2);
    let day = '0' + initialDay.getDate();
    day = day.substring(day.length - 2);
    let min = '0' + initialDay.getMinutes();
    min = min.substring(min.length - 2);
    let hour = '0' + initialDay.getHours();
    hour = hour.substring(hour.length - 2);

    const fullDate = initialDay.getFullYear() + '-' + month + '-' + day;
    const fullTime = hour + ':' + min;

    console.log('fullDate: ', fullDate);
    console.log('fullTime: ', fullTime);
    const [startDate, setStartDate] = useState(fullDate);
    const [startTime, setStartTime] = useState(fullTime);
    const [endDate, setEndDate] = useState(fullDate);
    const [endTime, setEndTime] = useState(fullTime);
    const [otReason, setOtReason] = useState('');

    const otStartDateAndTime = startDate + ' ' + startTime;
    const otEndDateAndTime = endDate + ' ' + endTime;
    const otStartDay = new Date(otStartDateAndTime).toISOString();
    const otEndDay = new Date(otEndDateAndTime).toISOString();

    console.log('otReason:   ', otReason);
    console.log('otStartDay:   ', otStartDay);
    console.log('otEndDay:   ', otEndDay);

    async function applyOt() {
        if (otReason === '') {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'Please Enter The Reason',
            });
            return;
        }
        const res = await post('/applyOt', {
            otReason,
            otStartDay,
            otEndDay,
        });
        const json = await res.json();

        if (json.double) {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'Multiple Application',
            });
        } else if (json.small) {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'End Time Must More Than The Start Time',
            });
        } else if (json.result) {
            Swal.fire({
                icon: 'success',
                title: 'Successful Application',
            });
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
            });
        }
    }

    return (
        <div className='otList-content'>
            <div className='otList-submitArea'>
                <div className='otList-name'>Apply For OT</div>
                <div className='chooseOtListArea'>
                    <div className='otStartDateArea'>
                        <p>Start Time</p>
                        <div className='otStartDateContent'>
                            <input type='date' id='otStartDate' value={startDate} onChange={(event) => setStartDate(event.currentTarget.value)} />
                            <input type='time' id='otEndTime' value={startTime} onChange={(event) => setStartTime(event.currentTarget.value)} />
                        </div>
                    </div>
                    <div className='otEndDateArea'>
                        <p>End Time</p>
                        <div className='otEndDateContent'>
                            <input type='date' id='otStartDate' value={endDate} onChange={(event) => setEndDate(event.currentTarget.value)} />
                            <input type='time' id='otEndTime' value={endTime} onChange={(event) => setEndTime(event.currentTarget.value)} />
                        </div>
                    </div>
                    <div className='applyOtReason'>
                        <p>Reason</p>
                        <input
                            className='otReason'
                            autoComplete='off'
                            type='input'
                            name='otReason'
                            placeholder='Enter The Reason'
                            value={otReason}
                            onChange={(event) => setOtReason(event.currentTarget.value)}
                        />
                    </div>
                </div>
                <div className='applyOtButtonArea'>
                    <button className='applyOt-button' type='submit' onClick={applyOt}>
                        Apply
                    </button>
                </div>
            </div>
        </div>
    );
}

export default OtList;
