import React from 'react';
import './ClockInList.scss';
import { useEffect, useState } from 'react';
import { get, post } from '../../api';
import Swal from 'sweetalert2';
import { useDispatch } from 'react-redux';
import { clockInSuccess, clockOutSuccess } from '../../redux/heartBeat/action';

export function ClockInList() {
    let dispatch = useDispatch();
    const [clockData, setClockData] = useState<{ id: number; timer: string } | null>(null);
    console.log('check ', clockData?.timer);

    let today = new Date(clockData?.timer as any);

    let month = '0' + (today.getMonth() + 1);
    month = month.substring(month.length - 2);
    let day = '0' + today.getDate();
    day = day.substring(day.length - 2);

    let min = '0' + today.getMinutes();
    min = min.substring(min.length - 2);
    let hour = '0' + today.getHours();
    hour = hour.substring(hour.length - 2);
    const time = today.getFullYear() + '-' + month + '-' + day + '     ' + hour + ':' + min;

    const [clockStatus, setClockStatus] = useState('');

    async function loadClockDate() {
        const res = await get('/loadClockData');
        const json = await res.json();
        setClockStatus(json.statusMsg);
        setClockData(json);
    }

    async function clockIn() {
        const res = await post('/clockIn');
        const json = await res.json();
        console.log('clock in json: ', json);
        if (json.result) {
            console.log('Clock In Success');

            dispatch(clockInSuccess(json.checkin_time));

            Swal.fire({
                icon: 'success',
                title: 'Clock In Success',
                text: `Clock In : ${time}`,
            });
        } else if (json.result === false) {
            console.log('Double Clock In');
            Swal.fire({
                icon: 'warning',
                title: 'Double Clock In',
                text: 'Please Clock Out',
            });
        }
        loadClockDate();
    }

    async function clockOut() {
        const res = await post('/clockOut', {
            clockInId: clockData?.id,
        });
        const json = await res.json();
        console.log('clock out json result ', json);

        if (json.result) {
            Swal.fire({
                icon: 'success',
                title: 'Clock Out Success',
                text: `Clock Out : ${time}`,
            });
            dispatch(clockOutSuccess());
        } else if (json.result === false) {
            Swal.fire({
                icon: 'warning',
                title: 'Double Clock Out',
                text: 'Please Clock In',
            });
        }
        loadClockDate();
    }

    useEffect(() => {
        loadClockDate();

        //console.log(`today get hours ${today.getHours()}`);
    }, []);

    return (
        <div className='clockInPage-content'>
            <div className='clockIn-content'>
                <div className='timing'>
                    <div className='top-time'>Clock In</div>

                    <div className='text-time'>
                        Last Clock {clockStatus} : {time}
                        {/* {hour !== 'aN' ? '' : `Last Clock${time}`} */}
                    </div>
                </div>

                <div className='clockButton'>
                    <button className='clock-In' onClick={clockIn}>
                        Clock In
                    </button>

                    <button className='clock-Out' onClick={clockOut}>
                        Clock Out
                    </button>
                </div>
            </div>
        </div>
    );
}

export default ClockInList;
