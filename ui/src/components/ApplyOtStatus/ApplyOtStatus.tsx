import React, { useState } from 'react';
import { get } from '../../api';
import './ApplyOtStatus.scss';
import { formatDateTime } from '../../format';
import Swal from 'sweetalert2';

export function ApplyOtStatus() {
    const [fullDate] = formatDateTime(new Date().toISOString()).split(' ');
    const [startDate, setStartDate] = useState(fullDate);
    const [endDate, setEndDate] = useState(fullDate);

    const start = new Date(startDate + ' 00:00');
    const end = new Date(endDate + ' 00:00');

    end.setTime(end.getTime() + 1000 * 60 * 60 * 24);

    const startStr = start.toISOString();
    const endStr = end.toISOString();

    async function getAllOtStatus() {
        console.log('get ot status');

        if (startDate > endDate) {
            console.log('End Date Must More Than The Start Date');
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'End Date Must More Than The Start Date',
            });
            return;
        }

        const res = await get(`/getOtData?start=${startStr}&end=${endStr}`);
        const json = await res.json();
        setGetOtStatus(json.staffOtData);
    }

    type otDataType = {
        id: number;
        reason: string;
        status: string;
        start_time: string;
        end_time: string;
        created_at: string;
        updated_at: string;
    };

    const [getOtStatus, setGetOtStatus] = useState<otDataType[]>([]);

    // useEffect(() => {
    //     (async () => {
    //         const res = await get('/getOtStatus');
    //         const json = await res.json();
    //         setGetOtStatus(json.otData);
    //     })();
    // }, []);

    return (
        <div className='staffOtStatusList-header'>
            <div className='staffOtStatusList-Area'>
                <div className='staffOtStatusList-name'>Apply OT Stauts</div>
                <div className='submitOtDateArea'>
                    <div className='otStartArea'>
                        <div className='otStartAreaName'>Start Time</div>
                        <input type='date' id='startDate' value={startDate} onChange={(event) => setStartDate(event.currentTarget.value)} />
                    </div>
                    <div className='otEndArea'>
                        <div className='otEndAreaName'>End Time</div>
                        <input type='date' id='startDate' value={endDate} onChange={(event) => setEndDate(event.currentTarget.value)} />
                    </div>
                    <button className='apply-button' type='submit' onClick={getAllOtStatus}>
                        Apply
                    </button>
                </div>
                <div className='staffOtStatusList-content desktop'>
                    <table>
                        <tr>
                            <th className='staffOtStatusList-created_at'>Application Date</th>
                            <th className='staffOtStatusList-start_time'>Start Time</th>
                            <th className='staffOtStatusList-end_time'>Finish Time</th>
                            <th className='staffOtStatusList-reason'>Reason</th>
                            <th className='staffOtStatusList-status'>Status</th>
                            <th className='staffOtStatusList-lastUpdate'>Latest Update</th>
                        </tr>

                        {getOtStatus.map((ot) => (
                            <tr>
                                <td className='otType-item'>{formatDateTime(ot.created_at)}</td>
                                <td className='otType-item'>{formatDateTime(ot.start_time)}</td>
                                <td className='otType-item'>{formatDateTime(ot.end_time)}</td>
                                <td className='otType-item'>{ot.reason}</td>
                                <td className='otType-item'>{ot.status}</td>
                                <td className='otType-item'>{formatDateTime(ot.updated_at)}</td>
                            </tr>
                        ))}
                    </table>
                </div>

                <div className='staffOtStatusList-content mobile'>
                    <table>
                        <tr>
                            <th className='staffOtStatusList-created_at'>Application Date</th>
                            <th className='staffOtStatusList-reason'>Reason</th>
                            <th className='staffOtStatusList-status'>Status</th>
                        </tr>

                        {getOtStatus.map((ot) => (
                            <tr>
                                <td className='otType-item'>{formatDateTime(ot.created_at)}</td>
                                <td className='otType-item'>{ot.reason}</td>
                                <td className='otType-item'>{ot.status}</td>
                            </tr>
                        ))}
                    </table>
                </div>
            </div>
        </div>
    );
}

export default ApplyOtStatus;
