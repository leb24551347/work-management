import React, { useState } from 'react';
import { useEffect } from 'react';
import { post, get } from '../../api';
import './VacationList.scss';
import Swal from 'sweetalert2';
import { formatDateTime } from '../../format';

export function VacationList() {
    const [fullDate, fullTime] = formatDateTime(new Date().toISOString()).split(' ');

    const [startDate, setStartDate] = useState(fullDate);
    const [startTime, setStartTime] = useState(fullTime);
    const [endDate, setEndDate] = useState(fullDate);
    const [endTime, setEndTime] = useState(fullTime);
    const [vacationId, setVacationId] = useState('');
    const [vacations, setVacations] = useState<{ id: number; vacation_name: string }[]>([]);
    const startDateAndTime = startDate + ' ' + startTime;
    const endDateAndTime = endDate + ' ' + endTime;

    const startDay = new Date(startDateAndTime).toISOString();
    const endDay = new Date(endDateAndTime).toISOString();

    async function applyVacation() {
        if (vacationId === '') {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'Please Choose The Vacation type',
            });
            return;
        }
        const res = await post('/applyVacation', {
            startDay,
            endDay,
            vacationId,
        });
        const json = await res.json();

        if (json.double) {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'Multiple Application',
            });
        } else if (json.small) {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
                text: 'End Time Must More Than The Start Time',
            });
        } else if (json.result) {
            Swal.fire({
                icon: 'success',
                title: 'Successful Application',
            });
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Application Failed',
            });
        }
    }

    useEffect(() => {
        (async () => {
            console.log('load vacation type fetch');
            const res = await get('/vacation');
            const json = await res.json();

            setVacations(json);
        })();
    }, []);

    return (
        <div className='vacationList-content'>
            <div className='vacationList-submitArea'>
                <div className='vacationList-name'>Apply For Vacation</div>
                <div className='chooseArea'>
                    <div className='startDateArea'>
                        <p>Start Time</p>
                        <div className='startDateContent'>
                            <input type='date' id='startDate' value={startDate} onChange={(event) => setStartDate(event.currentTarget.value)} />
                            <input type='time' id='endTime' value={startTime} onChange={(event) => setStartTime(event.currentTarget.value)} />
                        </div>
                    </div>
                    <div className='endDateArea'>
                        <p>End Time</p>
                        <div className='endDateContent'>
                            <input type='date' id='startDate' value={endDate} onChange={(event) => setEndDate(event.currentTarget.value)} />
                            <input type='time' id='endTime' value={endTime} onChange={(event) => setEndTime(event.currentTarget.value)} />
                        </div>
                    </div>

                    <div className='TypeOfVacationArea'>
                        <p>Type Of Vacation</p>
                        <select className='vacationId-list' value={vacationId} onChange={(event) => setVacationId(event.currentTarget.value)}>
                            <option className='type-item' value={''}>
                                Choose Vacation Type:
                            </option>
                            {vacations.map((vacation) => (
                                <option key={vacation.id} className='type-item' value={vacation.id}>
                                    {vacation.vacation_name}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className='applyButtonArea'>
                    <button className='apply-button' type='submit' onClick={applyVacation}>
                        Apply
                    </button>
                </div>
            </div>
        </div>
    );
}

export default VacationList;
