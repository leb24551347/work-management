import React from 'react';
import './StaffList.scss';

export function StaffList() {
    return (
        <div className='staffList'>
            <div className='staffList-header'>
                <h4>Staff List</h4>
            </div>

            <div className='staffList-top'>
                <div className='staffId'>Id</div>
                <div className='staffName'>Name</div>
                <div className='phoneNumber'>Phone Number</div>
            </div>
        </div>
    );
}

export default StaffList;
