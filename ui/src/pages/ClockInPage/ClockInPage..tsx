import React, { useState } from 'react';
import './ClockInPage.scss';

import ClockInList from '../../components/CLockInList/ClockInList';
import VacationList from '../../components/VacationList/VacationList';
import OtList from '../../components/OtList/otList';
import ApplyOtStatus from '../../components/ApplyOtStatus/ApplyOtStatus';
import ApplyVacationStatus from '../../components/ApplyVacationStatus/ApplyVacationStatus';
import AttendanceList from '../../components/Attendance/AttendanceList';
import { Link, Route } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import { HomePage } from '../HomePage/HomePage';

export function ClockInPage() {
    const [currentPage, setCurrentPage] = useState('vacation');
    return (
        <div className='clockInPage-content'>
            <ClockInList />

            <div className='clockIn-change desktop'>
                <div className='attendance-change' onClick={() => setCurrentPage('attendance')}>
                    Attendance List
                </div>
                <div className='space-line'>|</div>
                <div className='ot-change' onClick={() => setCurrentPage('ot')}>
                    Apply For OT
                </div>
                <div className='space-line'>|</div>
                <div className='otStatus-change' onClick={() => setCurrentPage('otStatus')}>
                    Apply OT Status
                </div>
                <div className='space-line'>|</div>

                <div className='vacation-change' onClick={() => setCurrentPage('vacation')}>
                    Apply For Vacation
                </div>
                <div className='space-line'>|</div>
                <div className='vacationStatus-change' onClick={() => setCurrentPage('vacationStatus')}>
                    Apply Vacation Status
                </div>
                <div className='space-line'>|</div>
                <Link className='overRall-change' to='/homepage'>
                    Over All
                </Link>
            </div>

            <div className='clockIn-change mobile'>
                <div className='attendance-change' onClick={() => setCurrentPage('attendance')}>
                    Attendance List
                </div>

                <div className='ot-change' onClick={() => setCurrentPage('ot')}>
                    Apply For OT
                </div>

                <div className='otStatus-change' onClick={() => setCurrentPage('otStatus')}>
                    Apply OT Status
                </div>

                <div className='vacation-change' onClick={() => setCurrentPage('vacation')}>
                    Apply For Vacation
                </div>

                <div className='vacationStatus-change' onClick={() => setCurrentPage('vacationStatus')}>
                    Apply Vacation Status
                </div>

                <Link className='overRall-change' to='/homepage'>
                    Over All
                </Link>
            </div>

            {currentPage === 'attendance' && <AttendanceList />}
            {currentPage === 'ot' && <OtList />}
            {currentPage === 'otStatus' && <ApplyOtStatus />}
            {currentPage === 'vacation' && <VacationList />}
            {currentPage === 'vacationStatus' && <ApplyVacationStatus />}

            <Switch>
                <Route path='/homepage' exact>
                    <HomePage />
                </Route>
            </Switch>
        </div>
    );
}

export default ClockInPage;
