import React, { useState } from 'react';
import './ChangePasswordPage.scss';
import { post } from '../../api';
import Swal from 'sweetalert2';

export function ChangePasswordPage() {
    const [repeatPassword, setRepeatPassword] = useState('');
    const [password, setPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');

    return (
        <div className='newPasswordContent'>
            <div className='newPasswordArea'>
                <h3 className='newPassword'>Change Password</h3>

                <input
                    className='changePasswordPassword'
                    type='password'
                    name='password'
                    placeholder='Password'
                    value={password}
                    onChange={(event) => setPassword(event.currentTarget.value)}
                />
                <input
                    className='changeNewPassword'
                    type='password'
                    name='changePassword'
                    placeholder='New Password'
                    value={newPassword}
                    onChange={(event) => setNewPassword(event.currentTarget.value)}
                />
                <input
                    className='changePasswordStaffName'
                    type='password'
                    name='Repeat Password'
                    autoComplete='off'
                    placeholder='Repeat Password'
                    value={repeatPassword}
                    onChange={(event) => setRepeatPassword(event.currentTarget.value)}
                />

                <div className='changePasswordSubmit'>
                    <button
                        className='changePassword'
                        type='submit'
                        id='createUser'
                        onClick={async () => {
                            if (newPassword !== repeatPassword) {
                                console.log('password not match');
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Password Not Match',
                                    text: 'Please Enter Again',
                                });
                                return;
                            }

                            const res = await post('/changePassword', {
                                password,
                                repeatPassword,
                                newPassword,
                            });
                            const json = await res.json();

                            if (json.result) {
                                console.log('Password Change Successful');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Password Change Successful',
                                });
                            } else {
                                console.log('Password Incorrect');
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Password Incorrect',
                                    text: 'Please Enter Again',
                                });
                            }
                        }}
                    >
                        Submit
                    </button>
                </div>
            </div>
        </div>
    );
}

export default ChangePasswordPage;
