import React from 'react';
import './StaffManagementPage.scss';
import { StaffList } from '../components/StaffList';

export function StaffManagementPage() {
    return (
        <div className='content'>
            <p>staff management page，睇員工OVERRALL</p>
            <div className='staffManagement-staffList'>
                <StaffList />
            </div>
        </div>
    );
}

export default StaffManagementPage;
