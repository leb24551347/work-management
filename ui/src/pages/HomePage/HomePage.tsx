// import React, { useState } from 'react';
import LeaveList from '../../components/HomePage/LeaveList/LeaveList';
import SalaryList from '../../components/HomePage/SalaryList/SalaryList';
import './HomePage.scss';
import { FaHome } from 'react-icons/fa';
import DropDownMonth from '../../components/HomePage/Dropdown/DropDownMonth';
import { useEffect, useState } from 'react';
import { get } from '../../api';
// import DropDownYear from '../../components/HomePage/Dropdown/DropDownYear';
// import { useEffect } from 'react';
// import { useState } from 'react';

// test area

// type MonthlyData = {
//     salary: number;
//     bonus: number;
//   };

export function HomePage() {
    
    
    const [month, setMonth] = useState<string>('1');
    // useEffect(() => {
    //     console.log(`parent is change state - current is ${parentCurrentMonth}`)
    // }, [parentCurrentMonth]
    // );

    // const parentHandleChangeMonth = () => {

    // }
    const [employeeInfo, setEmployeeInfo] = useState('1');

    async function getEmployeeData(month: string) {
        console.log('get month info', {month});
    
        // let date = new Date(selectMonth + ' 00:00');
        const res = await get(`/EmployeeInfo?start=${month}`);
        const json = await res.json();
        setEmployeeInfo(json)
        console.log("🚀 ~ file: HomePage.tsx ~ line 40 ~ useEffect ~ json", json);
        // setSelectedMonth(json)
        // setGetMonthInfo(json)
      }

      useEffect(() => {
        if (month)
          getEmployeeData(month);
      }, [month]
      );
    
    return (
        <div className='home_title'>
            <div className='homePageName'>
                <FaHome />
            </div>
            {/* <div>Salary: {monthlyData?.salary}</div>
            <div>Bonus: {monthlyData?.bonus}</div> */}
            {/* <DropDownYear /> */}
            {/* <DropDownMonth currentMonth={parentCurrentMonth} changeCurrentMonth={() => parentHandleChangeMonth()} /> */}

            {/* this is moving the state up parent demo */}
            {/* <DropDownMonth currentMonth={parentCurrentMonth} changeCurrentMonth={setParentCurrentMonth}/> */}
            {/* this is passing the state down to children demo */}
            {/* <SalaryList monthSalary={parseInt(parentCurrentMonth)} /> */}
            <DropDownMonth onChangeMonth={setMonth} month={month} />
            
             
            <SalaryList month={month}/>
            <LeaveList month={month}/>
            
        </div>
    );
}

export default HomePage;
