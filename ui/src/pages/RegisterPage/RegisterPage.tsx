import React, { useState } from 'react';
import './RegisterPage.scss';
import { useEffect } from 'react';
import { get, post } from '../../api';
import Swal from 'sweetalert2';

export function RegisterPage() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [checkRegistrationPassword, setCheckRegistrationPassword] = useState('');
    const [staffSalary, setStaffSalary] = useState('');
    const [authorityLevel, setAuthorityLevel] = useState('');
    const [departmentId, setDepartmentId] = useState('');
    const [departments, setDepartments] = useState<{ id: number; department_name: string }[]>([]);

    useEffect(() => {
        (async () => {
            const res = await get('/department');
            const json = await res.json();
            setDepartments(json);
        })();
    }, []);

    return (
        <div className='registerContent'>
            <div className='registerPageArea'>
                <h3 className='registerName'>Create Staff</h3>

                <input
                    className='registerStaffName'
                    type='input'
                    name='username'
                    autoComplete='off'
                    placeholder='Staff Name'
                    value={username}
                    onChange={(event) => setUsername(event.currentTarget.value)}
                />
                <input
                    className='registerPassword'
                    type='password'
                    name='password'
                    placeholder='Password'
                    value={password}
                    onChange={(event) => setPassword(event.currentTarget.value)}
                />
                <input
                    className='checkRegistrationPassword'
                    type='password'
                    name='checkRegistrationPassword'
                    placeholder='Repeat password'
                    value={checkRegistrationPassword}
                    onChange={(event) => setCheckRegistrationPassword(event.currentTarget.value)}
                />

                <select className='departmentId-list' value={departmentId} onChange={(event) => setDepartmentId(event.currentTarget.value)}>
                    <option className='type-item' value={''}>
                        Choose Department:
                    </option>
                    {departments.map((department) => (
                        <option key={department.id} className='type-item' value={department.id}>
                            {department.department_name}
                        </option>
                    ))}
                </select>
                <select className='authority-level' value={authorityLevel} onChange={(event) => setAuthorityLevel(event.currentTarget.value)}>
                    <option className='type-item' value={''}>
                        Choose Authority Level:
                    </option>
                    <option className='staffLevel'>1</option>
                    <option className='staffLevel'>2</option>
                    <option className='staffLevel'>3</option>
                    <option className='staffLevel'>4</option>
                </select>

                <input
                    className='staffSalary'
                    type='text'
                    name='staffSalary'
                    placeholder='Staff Salary'
                    value={staffSalary}
                    onChange={(event) => setStaffSalary(event.currentTarget.value)}
                />

                <div className='registerSubmit'>
                    <button
                        className='register'
                        type='submit'
                        id='createUser'
                        onClick={async () => {
                            if (password !== checkRegistrationPassword) {
                                console.log('Password Not Match');
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Password Not Match',
                                    text: 'Please Enter Again',
                                });
                                return;
                            } else if (departmentId === '') {
                                console.log('Department not find');
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Department Not Find',
                                    text: 'Please Choose Department',
                                });
                                return;
                            } else if (authorityLevel === '') {
                                console.log('authorityLevel not find');
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Authority Level Not Find',
                                    text: 'Please Choose Authority Level',
                                });
                                return;
                            }

                            const res = await post('/users/register', {
                                username,
                                password,
                                checkRegistrationPassword,
                                departmentId,
                                staffSalary,
                                authorityLevel,
                            });
                            const json = await res.json();

                            if (json.result) {
                                console.log('Create Success');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Create Success',
                                });
                            } else {
                                console.log('Multiple Application');
                                Swal.fire({
                                    icon: 'warning',
                                    title: 'Multiple Application',
                                    text: 'Please Enter Again',
                                });
                            }
                        }}
                    >
                        Create
                    </button>
                </div>
            </div>
        </div>
    );
}

export default RegisterPage;
