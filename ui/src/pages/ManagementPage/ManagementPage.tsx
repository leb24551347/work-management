import React, { useState } from 'react';
// import { useEffect, useState } from 'react';
//import { StaffList } from '../../components/ManagementPage/StaffList/StaffList';
import './ManagementPage.scss';
//import { Route, Switch } from 'react-router-dom';
//import { Link } from 'react-router-dom';
import { AttendanceRecord } from '../../components/ManagementPage/AttendanceRecord/AttendanceRecord';
import { StaffList } from '../../components/ManagementPage/StaffList/StaffList';
import { Link, Route, Switch } from 'react-router-dom';
import { colors } from 'react-select/src/theme';
import HeartBeatDataList from '../../components/getHeartBeat/getHeartBeat';

export function ManagementPage() {
    const [currentButton, setCurrentButton] = useState('');

    return (
        <div>
            <HeartBeatDataList />
            <div className='content'>
                <Link to='/ManagementPage/attendancerecord'>
                    <button
                        style={{ backgroundColor: currentButton === 'attendance' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)' }}
                        onClick={() => setCurrentButton('attendance')}
                    >
                        Attendance Record
                    </button>
                </Link>

                <Link to='/ManagementPage/StaffList'>
                    <button
                        style={{ backgroundColor: currentButton === 'staff list' ? '#f2b463' : 'rgba(117, 113, 113, 0.2)' }}
                        onClick={() => setCurrentButton('staff list')}
                    >
                        Staff List
                    </button>
                </Link>
            </div>

            <Switch>
                <Route path='/ManagementPage/attendancerecord'>
                    <AttendanceRecord />
                </Route>

                <Route path='/ManagementPage/StaffList'>
                    <StaffList />
                </Route>

                <Route path='/ManagementPage/'>
                    <AttendanceRecord />
                </Route>
            </Switch>
        </div>
    );
}
