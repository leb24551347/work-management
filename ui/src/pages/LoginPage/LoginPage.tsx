import React, { useState } from 'react';
import './LoginPage.scss';
import { loginSuccess } from '../../redux/auth/action';
import { useDispatch } from 'react-redux';
import { FaMix } from 'react-icons/fa';
import { ImEnter } from 'react-icons/im';
import { push } from 'connected-react-router';
import Swal from 'sweetalert2';
import { post } from '../../api';

export function LoginPage() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-right',

        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
        },
    });

    function validateForm() {
        return username.length > 0 && password.length > 0;
    }
    async function loginAction() {
        if (username === '' && password === '') {
            console.log('Cannot Find Username and Password');
            Swal.fire({
                icon: 'warning',
                title: 'Cannot Find Username and Password',
                text: 'Please Enter Username and Password',
            });
            return;
        }
        if (username === '') {
            console.log('Cannot Find Username');
            Swal.fire({
                icon: 'warning',
                title: 'Cannot Find Username',
                text: 'Please Enter Username',
            });
            return;
        } else if (password === '') {
            console.log('Cannot Find Password');
            Swal.fire({
                icon: 'warning',
                title: 'Cannot Find Password',
                text: 'Please Enter Password',
            });
            return;
        }

        const res = await post('/users/login', {
            username,
            password,
        });

        const json = await res.json();

        if (json.result) {
            dispatch(loginSuccess(json.token, json.user));
            Toast.fire({
                icon: 'success',
                title: 'Signed in successfully',
            });
            dispatch(push('/homepage'));
        } else {
            console.log('Incorrect Username or Password');
            dispatch(push('/login'));
            Swal.fire({
                icon: 'warning',
                title: 'Incorrect Username or Password',
                text: 'Please Enter Again',
            });
        }
    }

    return (
        <div className='loginHeader'>
            <div className='logoArea'>
                <div className='logoImgLogo desktop'>
                    Work <br />
                    <FaMix />
                    anagement
                </div>

                <div className='logoImgLogo mobile' style={{ fontSize: '2rem' }}>
                    Work <FaMix />
                    anagement
                </div>
            </div>
            <div className='loginListArea'>
                <h3 className='loginName'>Sign In</h3>
                <input
                    className='LoginUsername'
                    autoComplete='off'
                    type='input'
                    name='username'
                    placeholder='Staff Name'
                    value={username}
                    onChange={(event) => setUsername(event.currentTarget.value)}
                />
                <input
                    className='LoginPassword'
                    type='password'
                    name='password'
                    placeholder='Password'
                    value={password}
                    onChange={(event) => setPassword(event.currentTarget.value)}
                />
                <div className='forgetPasswordArea'>
                    <button
                        className='forgetPassword'
                        onClick={async () => {
                            Swal.fire({
                                icon: 'info',
                                title: 'Please contact HR Team Alex or Email: Alex@tecky.com',
                            });
                        }}
                    >
                        Forget Password?
                    </button>
                </div>
                <div className='LoginSubmit'>
                    <button className='login' type='submit' id='loginUser' disabled={!validateForm} onClick={loginAction}>
                        <ImEnter /> Sign In
                    </button>
                </div>
            </div>
        </div>
    );
}

export default LoginPage;
