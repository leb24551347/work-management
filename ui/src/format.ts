export function formatDateTime(dateStr: string | null) {
    if (!dateStr) {
        return '-';
    }
    const initialDay = new Date(dateStr);

    let month = '0' + (initialDay.getMonth() + 1);
    month = month.substring(month.length - 2);
    let day = '0' + initialDay.getDate();
    day = day.substring(day.length - 2);
    let min = '0' + initialDay.getMinutes();
    min = min.substring(min.length - 2);
    let hour = '0' + initialDay.getHours();
    hour = hour.substring(hour.length - 2);
    const fullDate = initialDay.getFullYear() + '-' + month + '-' + day;
    const fullTime = hour + ':' + min;
    return fullDate + ' ' + fullTime;
}

export function convertToHHMM(dateStr: string) {
    const initialDay = new Date(dateStr);
    let min = '0' + initialDay.getMinutes();
    min = min.substring(min.length - 2);
    let hour = '0' + initialDay.getHours();
    hour = hour.substring(hour.length - 2);
    const fullTime = hour + ':' + min;
    return fullTime;
}

export function convertToYMD(dateStr: string) {
    const initialDay = new Date(dateStr);
    let month = '0' + (initialDay.getMonth() + 1);
    month = month.substring(month.length - 2);
    let day = '0' + initialDay.getDate();
    day = day.substring(day.length - 2);
    const fullDate = initialDay.getFullYear() + '-' + month + '-' + day;
    return fullDate;
}

export function formatDuration(duration: number | null) {
    if (!duration || duration < 0) {
        return '-';
    }

    let reminding = duration;

    let ms = reminding % 1000;
    reminding = (reminding - ms) / 1000;

    let second = reminding % 60;
    reminding = (reminding - second) / 60;

    let minute = reminding % 60;
    reminding = reminding - minute;

    let hour = reminding / 60;

    reminding = (reminding - hour) / 60;

    let day = reminding;
    console.log(`${day} day ${hour}  hrs ${minute} min`);

    let hourKey = '0' + hour;
    hourKey = hourKey.substring(hourKey.length - 2);
    let minuteKey = '0' + minute;
    minuteKey = minuteKey.substring(minuteKey.length - 2);
    const fullDate = hourKey + ' hours ' + minuteKey + ' mins';
    console.log('fulldate', fullDate);

    return fullDate;
}
