let origin = process.env.REACT_APP_BACKEND_URL;

if (window.location.origin !== 'http://localhost:3000') {
    origin = window.location.origin;
}

export function get(url: string) {
    return fetch(`${origin}/api/v1` + url, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
    });
}

export function post(url: string, body: any = {}) {
    return fetch(`${origin}/api/v1` + url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(body),
    });
}
