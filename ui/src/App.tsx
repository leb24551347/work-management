import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import { LoginPage } from './pages/LoginPage/LoginPage';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { Redirect, Route, Switch } from 'react-router-dom';
import { RegisterPage } from './pages/RegisterPage/RegisterPage';
import { ClockInPage } from './pages/ClockInPage/ClockInPage.';
import { HomePage } from './pages/HomePage/HomePage';
import { ChangePasswordPage } from './pages/ChangePasswordPage/ChangePasswordPage';
import { ManagementPage } from './pages/ManagementPage/ManagementPage';
import Dashboard from './components/Dashboard/Dashboard';
import { User } from 'common';
import { loginSuccess } from './redux/auth/action';
import { get, post } from './api';
import Swal from 'sweetalert2';
import { heartBeatSuccess } from './redux/heartBeat/action';

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-right',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
    },
});

function App() {
    const staff = useSelector((state: RootState) => state.auth.user);
    let token = localStorage.getItem('token');
    let dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            if (!token) {
                return;
            }
            try {
                const res = await get('/user');
                const json: User = await res.json();
                console.log('app.tsx json:  ', json);
                dispatch(loginSuccess(token, json));
            } catch (error) {
                console.log('app.tsx can not find token');
            }
        })();
    }, [dispatch, token]);

    const next_heatBeatTime = useSelector((state: RootState) => state.heartBeat.next_heatBeatTime);

    console.log('app.tsx next_heatBeatTime   ', new Date(next_heatBeatTime!));

    // useEffect(() => {
    //     async function loadHeartBeatTime() {
    //         const res = await get('/heartBeat_time');
    //         const json = await res.json();
    //         console.log('heart beat json data: ', json.time);

    //         if (json.result) {
    //             dispatch(heartBeatSuccess(json.time));
    //         }
    //     }
    //     loadHeartBeatTime();
    // }, []);

    useEffect(() => {
        if (next_heatBeatTime == null) {
            return;
        }

        let timer = setTimeout(function () {
            Swal.fire({
                title: 'Click Heart Beat',
                confirmButtonText: 'Send',
                timer: 10 * 1000,
                timerProgressBar: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log('app.tsx  comfirmed');
                    (async () => {
                        console.log('send heat beat result');
                        try {
                            const res = await post('/heartBeat');
                            const json = await res.json();

                            console.log('heart beat json data: ', json);

                            if (json.result) {
                                dispatch(heartBeatSuccess(json.lastHeartBeatTime));
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Heart Beat Successful',
                                });
                            } else {
                                Toast.fire({
                                    icon: 'warning',
                                    title: 'Heart Beat Fail',
                                });
                            }
                        } catch (error) {
                            console.log('can not send heat beat result');
                        }
                    })();
                } else if (result.dismiss === Swal.DismissReason.timer) {
                    (async () => {
                        console.log('send heat beat result');
                        try {
                            await post('/clockOut');
                            console.log('you have clocked out');
                            Swal.fire({
                                icon: 'warning',
                                title: 'You Have Clocked Out',
                            });
                        } catch (error) {
                            console.log('can not send heat beat result');
                        }
                    })();

                    console.log('I was closed by the timer');
                }
                return;
            });
        }, next_heatBeatTime - Date.now());
        // console.log(timer, 'timer');
        // console.log('interf     ', next_heatBeatTime - Date.now());

        return () => clearTimeout(timer);
    }, [next_heatBeatTime, dispatch]);

    // TODO: TEST AREA
    if (!token) {
        return <LoginPage />;
    }

    return (
        <div className='App'>
            {staff ? (
                <div>
                    <Dashboard />
                    <div className='changePageFunction'>
                        <Switch>
                            <Route path='/' exact>
                                {token ? <Redirect to='/homepage' /> : <LoginPage />}
                            </Route>

                            <Route path='/register' exact>
                                <RegisterPage />
                            </Route>
                            <Route path='/clockIn' exact>
                                <ClockInPage />
                            </Route>

                            <Route path='/homepage' exact>
                                <HomePage />
                            </Route>

                            <Route path='/changePassword' exact>
                                <ChangePasswordPage />
                            </Route>

                            <Route path='/login' exact>
                                <LoginPage />
                            </Route>

                            <Route path='/ManagementPage'>
                                <ManagementPage />
                            </Route>

                            <Route path='/management/:list/:tab'>
                                <ManagementPage />
                            </Route>
                        </Switch>
                    </div>
                </div>
            ) : (
                <div>
                    <LoginPage />
                </div>
            )}
        </div>
    );
}

export default App;
