import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { createBrowserHistory } from 'history';
// import { staffReducer, StaffState } from './redux/staff/reducer';
import { AttendanceRecord, AttendanceRecordReducer } from './redux/ManagementPage/AttendanceRecord/reducer';
import { authReducer, AuthState } from './redux/auth/reducers';
import { StaffList, StaffListReducer } from './redux/ManagementPage/StaffList/reducer';
import { ClockState, clockReducer } from './redux/heartBeat/reducer';

export const history = createBrowserHistory();

export interface RootState {
    router: RouterState;
    AttendanceRecord: AttendanceRecord;
    StaffList:StaffList;
    auth: AuthState;
    
    heartBeat: ClockState;
}

const rootReducer = combineReducers<RootState>({
    router: connectRouter(history),
    AttendanceRecord: AttendanceRecordReducer,
    StaffList:StaffListReducer,
    auth: authReducer,
    heartBeat: clockReducer,
});

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}
type IRootAction = CallHistoryMethodAction;
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<RootState, IRootAction, {}, {}>(rootReducer, composeEnhancers(applyMiddleware(routerMiddleware(history))));
