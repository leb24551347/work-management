import { ClockActions } from './action';

export interface ClockState {
    next_heatBeatTime: number | null;
}

const initialState: ClockState = {
    next_heatBeatTime: null,
};

let second = 1000;
// let minute = second * 60;
// let hour = minute * 60;

export const clockReducer = (state: ClockState = initialState, action: ClockActions): ClockState => {
    console.log('clockReducer action above', { action, state });

    if (action.type === '@@heartBeat/CLOCKOUT_SUCCESS') {
        return {
            ...state,
            next_heatBeatTime: null,
        };
    } else if (action.type === '@@heartBeat/HEARTBEAT_SUCCESS' || action.type === '@@heartBeat/CLOCKIN_SUCCESS') {
        console.log('set heart beat time ', new Date(action.time));
        return {
            ...state,
            next_heatBeatTime: new Date(action.time).getTime() + second * 10,
        };
    }
    return state;
    // switch (action.type) {
    //     case '@@heartBeat/CLOCKOUT_SUCCESS':
    //         return { next_heatBeatTime: null };
    //     case '@@heartBeat/HEARTBEAT_SUCCESS':
    //     case '@@heartBeat/CLOCKIN_SUCCESS':
    //         return {
    //             next_heatBeatTime: new Date(action.time).getTime() + hour * 2,
    //         };
    // }
};
