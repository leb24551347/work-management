export function heartBeatSuccess(time: number) {
    console.log('this is clock action');
    return {
        type: '@@heartBeat/HEARTBEAT_SUCCESS' as '@@heartBeat/HEARTBEAT_SUCCESS',
        time,
    };
}

export function clockInSuccess(time: number) {
    return {
        type: '@@heartBeat/CLOCKIN_SUCCESS' as '@@heartBeat/CLOCKIN_SUCCESS',
        time,
    };
}

export function clockOutSuccess() {
    return {
        type: '@@heartBeat/CLOCKOUT_SUCCESS' as '@@heartBeat/CLOCKOUT_SUCCESS',
    };
}

export type ClockActions = ReturnType<typeof heartBeatSuccess> | ReturnType<typeof clockInSuccess> | ReturnType<typeof clockOutSuccess>;
