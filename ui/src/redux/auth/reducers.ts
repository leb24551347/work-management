import { User } from 'common';
import { AuthActions } from './action';

export interface AuthState {
    token: string | null;
    // isAuthenticated: boolean | null;
    user: User | null;
}

const initialState: AuthState = {
    // isAuthenticated: null,
    token: null,
    user: null,
    // department_id: null,
    // authority_level: null,
};

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
    console.log(action);
    console.log('authReducer action above');
    if (action.type === '@@auth/LOGIN_SUCCESS') {
        console.log('this is @@auth/LOGIN_SUCCESS @ authReducer');
        localStorage.setItem('token', action.token);
        return {
            ...state,
            // isAuthenticated: true,
            token: action.token,
            user: action.user,
            // department_id: action.department_id,
            // username: action.username,
            // authority_level: action.authority_level,
        };
    } else if (action.type === '@@auth/LOGOUT_SUCCESS') {
        console.log('this is @@auth/LOGOUT_SUCCESS @ authReducer');
        return {
            ...state,
            // isAuthenticated: false,
            user: null,
            token: null,
        };
    }

    return state;
};
