// import { push } from 'connected-react-router';
// import { Dispatch } from 'redux';
import { User } from '../../../../common/types';

export function loginSuccess(token: string, user: User) {
    console.log('this is action');
    // console.log(department_id, authority_level);
    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        token,
        user,
        // department_id: department_id,
        // username: username,
        // authority_level: authority_level,
    };
}

export function loginFailed() {
    return {
        type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
    };
}

export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT_SUCCESS' as '@@auth/LOGOUT_SUCCESS',
    };
}

export type AuthActions = ReturnType<typeof loginSuccess> | ReturnType<typeof loginFailed> | ReturnType<typeof logoutSuccess>;
