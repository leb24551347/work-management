import { SelectStaffAction } from "./action"


export interface StaffList{
    staffId:number
}

export const initialState:StaffList={
    staffId: 0
}

export const StaffListReducer = (
    state: StaffList = initialState,
    action: SelectStaffAction,
): StaffList => {
    if (action.type === '@@selectStaff') {
        return {
            ...state,
            staffId: action.staffId,
        }
    }
    return state
}