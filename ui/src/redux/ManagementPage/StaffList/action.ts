export function SelectStaff(staffId:number){

    return{
        type: '@@selectStaff' as '@@selectStaff',
        staffId
    }
}

export type SelectStaffAction = ReturnType<typeof SelectStaff>;