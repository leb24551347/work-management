export function SelectDate(selectDate:string){
    return{
        type: '@@selectDate' as '@@selectDate',
        selectDate,
    };
}





export type SelectDateAction = ReturnType<typeof SelectDate>;