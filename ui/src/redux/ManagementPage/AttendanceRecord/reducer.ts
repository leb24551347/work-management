import { SelectDateAction } from "./action";


export interface AttendanceRecord {
    currentPage: string,
    selectDate: string
}

export const initialState: AttendanceRecord = {
    currentPage: "",
    selectDate: "2021-01-01"
};

export const AttendanceRecordReducer = (
    state: AttendanceRecord = initialState,
    action: SelectDateAction,
): AttendanceRecord => {
    if (action.type === '@@selectDate') {
        return {
            ...state,
            selectDate: action.selectDate,
        }
    }
    return state
}

